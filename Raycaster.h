
#ifndef RAYCASTER_H
#define RAYCASTER_H


#include "Ray.h"

class Raycaster {
public:
    static Ray Spawn(double x, double y);
    static void RegisterCamera(Camera* cam);

    static void RegisterCollider(BoxColliderComponent* collider);
    static void ProcessCollision(Ray ray);
private:
    static std::vector<BoxColliderComponent*> registeredColliders;
    static Camera* camera;
};


#endif
