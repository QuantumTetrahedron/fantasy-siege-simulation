#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include "Object.h"
#include "BehaviourComponent.h"
#include "Battlefield.h"
#include "GFXengine.h"
#include "ResourceManager.h"
#include "Window.h"
#include "Input.h"
#include "Raycaster.h"
#include "UIcontroller.h"

const unsigned int screenWidth = 1024;
const unsigned int screenHeight = 768;

Input input;

Camera camera(screenWidth, screenHeight, glm::vec3(0.0f,0.0f,0.0f));

double deltaTime = 0.0f;
double lastFrame = 0.0f;

void ProcessMovement();

int main() {
    glfwInit();

    Window window(screenWidth, screenHeight, "Fantasy Combat Simulator");
    window.makeCurrentContext();

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    window.SetKeyCallback([](GLFWwindow* win, int key, int scancode, int action, int mode){
        if(action == GLFW_PRESS)
            input.PressKey(key);
        else if(action == GLFW_RELEASE)
            input.ReleaseKey(key);
    });

    window.SetScrollCallback([](GLFWwindow* win, double xoffset, double yoffset){
        input.SetScroll(yoffset);
    });

    window.SetMouseButtonCallback([](GLFWwindow* win, int button, int action, int mods){
        if(button == GLFW_MOUSE_BUTTON_LEFT){
            if(action == GLFW_PRESS)
                input.LeftMouseButtonPress();
            else if(action == GLFW_RELEASE)
                input.LeftMouseButtonRelease();
        }
    });

    window.SetMouseMoveCallback([](GLFWwindow* win, double x, double y){
       input.SetMousePosition(x, y);
    });

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
    glStencilMask(0x00);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    ResourceManager::LoadShader("shaders/default.vert" ,"shaders/default.frag" , "default");
    ResourceManager::LoadShader("shaders/instanced.vert" ,"shaders/default.frag" , "instanced");
    ResourceManager::LoadShader("shaders/default.vert", "shaders/simple.frag", "outline");
    ResourceManager::LoadShader("shaders/sprite.vert", "shaders/sprite.frag", "sprite");
    ResourceManager::LoadModel("models/field/field.obj" , "field");
    ResourceManager::LoadModel("models/water/water.obj" , "water");
    ResourceManager::LoadModel("models/wall/wall.obj" , "wall");
    ResourceManager::LoadModel("models/units/human_soldier/human_soldier.obj", "human_soldier");
    ResourceManager::LoadModel("models/units/human_commander/human_commander.obj", "human_commander");
    ResourceManager::LoadModel("models/units/orc_soldier/orc_soldier.obj", "orc_soldier");
    ResourceManager::LoadModel("models/units/orc_commander/orc_commander.obj", "orc_commander");
    ResourceManager::LoadTexture("textures/blank.png", false, "blank");
    ResourceManager::LoadTexture("textures/pathfinding.png", true, "pathfinding");
    ResourceManager::LoadTexture("textures/move.png", true, "moving");
    ResourceManager::LoadTexture("textures/aim.png", true, "aiming");
    ResourceManager::LoadTexture("textures/attack.png", true, "attacking");
    ResourceManager::LoadTexture("textures/flag.png", true, "flag");
    ResourceManager::LoadShader("shaders/text.vert", "shaders/text.frag", "text");

    GFXengine::Initialize();
    Battlefield::Load("Maps/map.png", "Maps/units.txt");

    GFXengine::AllocateInstanceBuffers();
    GFXengine::RegisterMainCamera(&camera);
    Raycaster::RegisterCamera(&camera);

    UIcontroller::Init(&camera);

    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);
    while (!glfwWindowShouldClose(window.get()))
    {
        double currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        ProcessMovement();
        camera.ProcessMouseScroll(input.GetScrollOffset());

        Battlefield::Update(deltaTime);
        Battlefield::ProcessInput(&input);
        UIcontroller::Update(deltaTime);

        input.ResetOffsets();
        glStencilMask(0xFF);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glStencilMask(0x00);
        GFXengine::Draw();
        glfwSwapBuffers(window.get());
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

void ProcessMovement(){
    if(input.GetKey(GLFW_KEY_W))
        camera.ProcessKeyboard(Camera::Movement::Forward, deltaTime);
    if(input.GetKey(GLFW_KEY_S))
        camera.ProcessKeyboard(Camera::Movement::Backward, deltaTime);
    if(input.GetKey(GLFW_KEY_A))
        camera.ProcessKeyboard(Camera::Movement::Left, deltaTime);
    if(input.GetKey(GLFW_KEY_D))
        camera.ProcessKeyboard(Camera::Movement::Right, deltaTime);
}