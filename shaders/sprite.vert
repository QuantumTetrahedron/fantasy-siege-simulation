#version 330 core
layout (location = 0) in vec2 position;

out vec2 texCoords;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 orthoProjection;
uniform bool is3d;

void main(){
	if(is3d)
		gl_Position = projection * view * model * vec4(position, 0.0f,1.0f);
	else
		gl_Position = orthoProjection * model * vec4(position.x, 1.0f-position.y, 0.0f,1.0f);
	texCoords = position;
}