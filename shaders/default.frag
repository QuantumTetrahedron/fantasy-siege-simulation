#version 330 core

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 fragColor;

uniform vec3 viewPos;

uniform vec3 colorTint;

uniform sampler2D texture_diffuse1;


void main()
{
    vec3 color = texture( texture_diffuse1, TexCoords ).rgb;
    vec3 ambient = 0.05 * color;
    vec3 lightDir = normalize(vec3(0.0, 1.0, 0.5));
    vec3 normal = normalize(Normal);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;
    vec3 viewDir = normalize(viewPos-FragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    vec3 specular = vec3(0.3) * spec;
    fragColor = vec4(colorTint*(ambient+diffuse)+specular, 1.0);
}
