
#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H

#include <string>
#include <glm/glm.hpp>
#include "Component.h"


class RenderComponent : public Component{
public:
    enum class RenderType{
        simple, instanced
    };

    RenderComponent(Object* parent, std::string modelName, RenderType type);

    void Update(double dt){}

    glm::mat4 GetModelMatrix() const;

    RenderType GetRenderType() const;
    std::string GetModelName() const;

    bool IsOutlined() const;
    void SetOutline(bool o, glm::vec3 color = glm::vec3(0.0f));
    glm::vec3 GetOutlineColor();
private:
    bool outlined;
    glm::vec3 outlineColor;
    std::string modelName;
    RenderType renderType;
};


#endif
