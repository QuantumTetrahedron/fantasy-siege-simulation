
#include <algorithm>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include "UImanager.h"
#include "ResourceManager.h"
#include "Object.h"

void UImanager::Draw() {
    Shader shader = ResourceManager::GetShader("sprite");
    shader.use();
    shader.setMat4("view", cam->GetViewMatrix());
    shader.setMat4("projection", cam->GetProjectionMatrix());
    shader.setMat4("orthoProjection", glm::ortho(0.0f,cam->GetScreenSize().x, cam->GetScreenSize().y, 0.0f));

    for(SpriteComponent* c : sprites){
        if(c->Is3D())
            renderer.DrawSprite(shader, c);
    }
    for(SpriteComponent* c : sprites){
        if(!c->Is3D())
            renderer.DrawSprite(shader, c);
    }

    shader = ResourceManager::GetShader("text");
    shader.use();
    shader.setMat4("projection", glm::ortho(0.0f,cam->GetScreenSize().x, cam->GetScreenSize().y, 0.0f));

    for(TextComponent* c : texts){
        textRenderer.RenderText(c);
    }
}

void UImanager::RegisterComponent(SpriteComponent *c) {
    sprites.push_back(c);

}

void UImanager::DeleteComponent(SpriteComponent *c) {
    sprites.erase(std::remove_if(std::begin(sprites), std::end(sprites), [c](SpriteComponent* comp){return comp == c;}), sprites.end());
}

void UImanager::RegisterMainCamera(Camera *c) {
    cam = c;
}

void UImanager::Init() {
    renderer.initRenderData();
    textRenderer.Init();
}

void UImanager::RegisterComponent(TextComponent *c) {
    texts.push_back(c);
}

void UImanager::DeleteComponent(TextComponent *c) {
    texts.erase(std::remove_if(std::begin(texts), std::end(texts), [c](TextComponent* comp){return comp == c;}), texts.end());
}
