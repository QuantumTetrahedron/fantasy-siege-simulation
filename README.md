# Symulacja oblężenia

Folder zawiera pliki źródłowe oraz wykorzystywane zasoby.

Instrukcja kompilacji dla systemu Linux

## 1. Instalacja wymaganych bibliotek

```
sudo apt-get install libglfw3-dev
sudo apt-get install libassimp-dev
sudo apt-get install libfreetype6-dev
sudo apt-get install libglm-dev
```

WAŻNE - Pliki źródłowe biblioteki freetype instalują się niepoprawnie podczas instalacji automatycznej (właściwie to instalują się tak jak powinny, natomiast nie tak jak życzyłby sobie twórca biblioteki). Struktura kodu biblioteki zmusza nas do zmienienia hierarchii plików.

```
sudo cp -r /usr/include/freetype2/freetype /usr/local/include/freetype
sudo cp /usr/include/freetype2/ft2build.h /usr/local/include/ft2build.h
sudo rm -r /usr/include/freetype2
```

## 2. Kompilacja oraz uruchomienie

```
cmake CMakeLists.txt
make
./FantasyCombatSimulator
```

Program przy uruchomieniu może sprawiać wrażenie, że nie odpowiada.
Dzieje się tak dlatego, że przed uruchomieniem obliczane są odległości dla każdej pary punktów na mapie. Podczas obliczania w terminalu powinny wyświetlać się komunikaty o postępie.

## 3. Instrukcja obsługi

Sterowanie kamerą:

WSAD - ruch

scroll - oddalanie/przybliżanie

lewy przycisk myszy - wycieranie jednostek

escape - anulowanie wyboru

spacja - kolejna faza symulacji

enter - automatyczne przewijanie faz - niezalecane przy dużej ilości żywych jednostek
