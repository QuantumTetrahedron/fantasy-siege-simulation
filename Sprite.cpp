
#include "Sprite.h"
#include "GFXengine.h"
#include "ResourceManager.h"

Sprite::Sprite()
: UIelement(glm::vec2(0.0f), glm::vec2(0.0f), nullptr){
}

void Sprite::SetColor(glm::vec4 color) {
    uIcomponent->SetColor(color);
}

Sprite::Sprite(glm::vec2 screenSize, std::string textureName, glm::vec2 pos, glm::vec2 size, glm::vec4 color, float rotation, UIelement* parent)
: UIelement(screenSize, pos, parent) {
    Texture texture = ResourceManager::GetTexture(textureName);
    uIcomponent = new SpriteComponent(this, texture, size, color, rotation);
    AddComponent(uIcomponent);
    GFXengine::RegisterUIcomponent(uIcomponent);
}

void Sprite::SetSize(glm::vec2 newSize) {
    uIcomponent->SetSize(newSize);
}

glm::vec2 Sprite::GetSize() const {
    return uIcomponent->GetSize();
}
