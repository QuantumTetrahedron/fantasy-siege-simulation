
#ifndef SPRITE_COMPONENT_H
#define SPRITE_COMPONENT_H

#include "Component.h"
#include "Texture.h"
#include <glm/glm.hpp>

class SpriteComponent : public Component {
public:
    enum class Space{
        _2D, _3D
    };

    SpriteComponent(Object* p, Texture tex, glm::vec2 size, glm::vec4 color, float rotation);
    SpriteComponent(Object* p, Texture tex, glm::vec2 size, glm::vec4 color, float yaw, float pitch);

    void Update(double dt) override {}

    glm::mat4 GetModelMatrix();

    glm::vec4 GetColor() const;
    void SetColor(glm::vec4 newColor);
    Texture GetTexture() const;
    bool Is3D() const;

    glm::vec2 GetSize();
    void SetSize(glm::vec2 newSize);
private:
    glm::vec2 size;
    glm::vec4 color;
    Texture texture;
    Space space;

    float rotation;
    float pitch;
};


#endif
