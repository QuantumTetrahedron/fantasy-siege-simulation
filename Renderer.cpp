
#include "Renderer.h"
#include "ResourceManager.h"
#include "Object.h"
#include <glm/gtc/matrix_transform.hpp>

void Renderer::Draw(const std::vector<RenderComponent *> &components, const Shader& shader) const {
    shader.use();
    shader.setVec3("colorTint", glm::vec3(1.0f));
    for(RenderComponent* c : components){
        shader.setMat4("model", c->GetModelMatrix());
        ResourceManager::GetModel(c->GetModelName()).Draw(shader);
    }
}

void Renderer::DrawInstanced(const std::vector<RenderComponent *> &components, const Shader& shader) const {
    if(components.empty()) return;
    shader.use();
    shader.setVec3("colorTint", glm::vec3(1.0f));
    std::vector<glm::mat4> matrices;
    std::transform(components.begin(), components.end(), std::back_inserter(matrices), [](RenderComponent* c){ return c->GetModelMatrix(); });
    Model m = ResourceManager::GetModel(components[0]->GetModelName());
    m.DrawInstanced(matrices, shader, matrices.size());
}

void Renderer::Draw(const RenderComponent *renderComponent, const Shader &shader, glm::vec3 color) const {
    shader.use();
    shader.setMat4("model", renderComponent->GetModelMatrix());
    shader.setVec3("colorTint", color);
    Model m = ResourceManager::GetModel(renderComponent->GetModelName());
    m.Draw(shader);
}

void Renderer::DrawAltered(const RenderComponent *renderComponent, const Shader &shader, glm::vec3 color, glm::vec3 posOffset, float rotOffset, glm::vec3 scale) {
    shader.use();
    shader.setVec3("colorTint", color);
    glm::mat4 model(1.0f);

    model = glm::translate(model, renderComponent->GetParent()->Position + posOffset);
    model = glm::rotate(model, glm::radians(rotOffset), glm::vec3(0.0f,1.0f,0.0f));
    model = glm::scale(model, scale);

    shader.setMat4("model", model);

    Model m = ResourceManager::GetModel(renderComponent->GetModelName());
    m.Draw(shader);
}
