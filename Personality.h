#ifndef PERSONALITY_H
#define PERSONALITY_H

#include <string>
#include <map>

class Personality {
public:
    void Set(std::string name, float value);
    float Get(std::string name) const;

    void Normalize();
private:

    std::map<std::string, float> weights;
};


#endif
