
#ifndef UICONTROLLER_H
#define UICONTROLLER_H

#include "Grid.h"
#include "Unit.h"
#include "Sprite.h"
#include "Battlefield.h"

class UIcontroller {
public:
    static void Init(Camera* cam);
    static void SetState(Battlefield::State state);

    static void SetSelection(Unit* newUnit);
    static void Deselect();
    static void UpdateGrid(Unit* u);

    static void Update(double dt);
private:
    static Grid grid;
    static Unit* selectedUnit;

    static std::vector<std::vector<glm::vec4>> ColorsFromPenalties(const std::vector<std::vector<float>>& penalties);
    static std::vector<std::vector<glm::vec4>> ColorsFromPath(const std::vector<glm::vec2>& path);
    static std::vector<std::vector<glm::vec4>> ColorsFromTargets(const std::vector<glm::vec2>& targets);
    static std::vector<std::vector<glm::vec4>> ColorsFromAttacks(const std::vector<std::pair<glm::vec2, int>> attacks);
    static std::vector<std::vector<glm::vec4>> ColorsFromCommandZone(const std::vector<float>& commandReasoning);
    static std::vector<std::vector<glm::vec4>> ColorsFromCommand();
    static std::string RaceToString(UnitComponent::Race race);
    static std::string ArchetypeToString(UnitComponent::Archetype arch);

    static void UpdateDataPanel();

    static std::map<std::string, UIelement*> elements;
    static Battlefield::State currentState;
};


#endif
