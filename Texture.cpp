
#include "Texture.h"

#include "stb_image.h"
#include <glad/glad.h>

Texture::Texture(const char *path, bool alpha)
: width(0), height(0), dataFormat(GL_RGB), internalFormat(GL_RGB), wrapS(GL_REPEAT), wrapT(GL_REPEAT), filterMin(GL_LINEAR_MIPMAP_LINEAR), filterMax(GL_LINEAR){
    glGenTextures(1, &id);
    if(alpha)
        internalFormat = GL_RGBA;
    stbi_set_flip_vertically_on_load(true);
    int width, height, nrChannels;
    unsigned char* image = stbi_load(path, &width, &height, &nrChannels, 0);
    if(nrChannels == 4)
        dataFormat = GL_RGBA;
    Generate(width, height, image);
    stbi_image_free(image);
}

void Texture::Bind() const {
    glBindTexture(GL_TEXTURE_2D, id);
}

void Texture::Generate(unsigned int w, unsigned int h, unsigned char *data) {
    width = w;
    height = h;
    glBindTexture(GL_TEXTURE_2D, id);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMin);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMax);

    glBindTexture(GL_TEXTURE_2D, 0);
}
