#define STB_IMAGE_IMPLEMENTATION

#include <iostream>
#include <fstream>
#include <sstream>
#include <GLFW/glfw3.h>
#include <algorithm>
#include "stb_image.h"

#include "Battlefield.h"
#include "Ray.h"
#include "Raycaster.h"
#include "Pathfinder.h"
#include "UIcontroller.h"
#include "GFXengine.h"

glm::vec2 Battlefield::size;
std::vector<Field*> Battlefield::fields;
std::vector<Unit*> Battlefield::units;
Battlefield::State Battlefield::state = State::Start;
bool Battlefield::autoPlay = false;
std::map<int, glm::vec2> Battlefield::flags;

void Battlefield::Load(const char *terrainMapFilePath, const char *unitMapFilePath) {
    LoadTerrainFromFile(terrainMapFilePath);
    LoadUnitsFromFile(unitMapFilePath);
}

void Battlefield::LoadTerrainFromFile(const char *terrainMapFilePath) {
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(false);
    unsigned char* data = stbi_load(terrainMapFilePath, &width, &height, &nrChannels, 0);
    if(data)
    {
        size = glm::vec2(width, height);
        fields.reserve(width*height);

        for(int i = 0; i < width*height*nrChannels; i+=nrChannels){
            int x = (int)glm::floor(i / nrChannels) % width;
            int y = (int)glm::floor(i / nrChannels) / width;

            Field::TerrainType c;
            int r = data[i];
            int g = data[i+1];
            int b = data[i+2];
            if(b == 255){
                if(r == 255 && g == 255)
                    c = Field::TerrainType::empty;
                else if(r == 0 && g == 0)
                    c = Field::TerrainType::water;
                else c = Field::TerrainType::unknown;
            }
            else{
                if(r == 0 && g == 0 && b == 0)
                    c = Field::TerrainType::wall;
                else
                    c = Field::TerrainType::unknown;
            }
            Field* f = new Field(2*x,2*y,c);
            fields.push_back(f);
        }
    }
    else{
        std::cerr << "Failed to load map " << terrainMapFilePath << std::endl;
    }
    Pathfinder::LoadMap(fields);
    stbi_set_flip_vertically_on_load(true);
}

void Battlefield::LoadUnitsFromFile(const char *unitMapFilePath) {
    std::string line;
    std::ifstream file(unitMapFilePath);
    while(std::getline(file, line)){
        std::string param;
        std::string value;

        std::istringstream stream(line);

        stream >> param;
        int side;

        if(param == "unit"){
            std::string name;
            int x,y;
            stream >> name >> x >> y;
            for(int i=0;i<name.size();++i)
                if(name[i]=='_') name[i] = ' ';
            Unit* u = Unit::Factory::Spawn(2*x,2*y,name);
            units.push_back(u);
        }
        else if(param == "commander"){
            std::string name;
            int x,y;
            stream >>name>> x >> y;
            for(int i=0;i<name.size();++i)
                if(name[i]=='_') name[i] = ' ';
            Unit* u = Unit::Factory::SpawnCommander(2*x,2*y,name);
            units.push_back(u);
        }
        else if(param == "flag"){
            int x,y;
            stream >> x >> y;
            flags.emplace(std::pair<int, glm::vec2>(side, glm::vec2(2*x,2*y)));
        }
        else if(param == "side"){
            stream >> value;
            Unit::Factory::SetParameter(param, value);
            std::istringstream ss(value);
            ss >> side;
        }
        else{
            stream >> value;
            Unit::Factory::SetParameter(param, value);
        }
    }
    file.close();
}

glm::vec2 Battlefield::GetSize() {
    return size;
}

Unit* Battlefield::getUnit(int x, int y){
    for(Unit* u : units){
        if(u->GetBehaviourComponent()->targetPosX == x && u->GetBehaviourComponent()->targetPosY == y)
            return u;
    }
    return nullptr;
}

void Battlefield::ProcessInput(Input* in) {

    if(in->MousePressed()){
        glm::vec2 mp = in->GetMousePosition();
        Ray ray = Raycaster::Spawn(mp.x, mp.y);
        Raycaster::ProcessCollision(ray);
        in->LeftMouseButtonRelease();
    }

    if(in->GetKey(GLFW_KEY_ENTER)){
        autoPlay = !autoPlay;
        in->ReleaseKey(GLFW_KEY_ENTER);
    }

    if(in->GetKey(GLFW_KEY_SPACE) || autoPlay)
    {
        if(state == State::Start || state == State::AttackingEnded){
            state = State::CommandPhase;
            IssueCommands();
            state = State::CommandPhaseEnded;
        }
        else if(state == State::CommandPhaseEnded){
            state = State::CalculatingMoves;
        }
        else if(state == State::CalculatingMovesEnded){
            state = State::Moving;
            MoveUnits();
        }
        else if(state == State::MovingEnded){
            state = State::FindingTargets;
            FindTargets();
            state = State::FindingTargetsEnded;
        }
        else if(state == State::FindingTargetsEnded){
            state = State::Attacking;
            Attack();
            state = State::AttackingEnded;
            EraseDead();
        }

        in->ReleaseKey(GLFW_KEY_SPACE);
    }

    if(in->GetKey(GLFW_KEY_ESCAPE)){
        UIcontroller::Deselect();
    }

    UIcontroller::SetState(state);
}

void Battlefield::Update(double dt) {
    bool someUnitsMoving = false;
    for(Unit* u : units){
        u->Update(dt);
        someUnitsMoving = someUnitsMoving || u->IsMoving();
    }
    if(state == State::Moving && !someUnitsMoving) {
        state = State::MovingEnded;
    }

    if(state == State::CalculatingMoves)
    {
        CalculatePenalties();
        state = State::CalculatingMovesEnded;
    }

    UIcontroller::SetState(state);
}

void Battlefield::CalculatePenalties() {
    for(Unit* u : units)
    {
        BehaviourComponent* bc = dynamic_cast<BehaviourComponent*>(u->GetComponent(CompType::behaviour));
        if(bc)
            bc->CalculatePrefferedPosition();
    }
}

void Battlefield::MoveUnits() {
    std::vector<std::pair<int, int>> order;
    for(int i=0;i<units.size();++i)
        order.emplace_back(units[i]->GetBehaviourComponent()->RollInitiative(), i);
    std::sort(order.begin(), order.end(), [](auto a, auto b){return a.first > b.first;});

    for(int i=0;i<order.size();++i)
        units[order[i].second]->Move();
}

bool Battlefield::HasUnit(int x, int y) {
    for(const Unit* u : units){
        if(u->GetBehaviourComponent()->targetPosX == x && u->GetBehaviourComponent()->targetPosY == y)
            return true;
    }
    return false;
}

bool Battlefield::TerrainIsTrespassable(int x, int y) {
    for(const Field* f : fields){
        if(f->Position.x == x && f->Position.z == y && f->isTrespassable())
            return true;
    }
    return false;
}

void Battlefield::FindTargets() {
    for(Unit* u : units){
        u->GetBehaviourComponent()->FindTargets();
    }
}

void Battlefield::Attack() {
    std::vector<std::pair<int, int>> order;
    for(int i=0;i<units.size();++i)
        order.emplace_back(units[i]->GetBehaviourComponent()->RollInitiative(), i);
    std::sort(order.begin(), order.end(), [](auto a, auto b){return a.first > b.first;});

    for(int i=0;i<order.size();++i)
        units[i]->GetBehaviourComponent()->Attack();
}

void Battlefield::EraseDead() {
    for(int i=0;i<units.size();++i){
    //for(Unit* u : units){
        if(units[i]->IsDead()){
            units[i]->CleanUp();
            delete units[i];
            units[i] = nullptr;
        }
    }
    units.erase(std::remove_if(units.begin(), units.end(), [](Unit* u){return u == nullptr;}), units.end());
}

glm::vec2 Battlefield::GetFriendlyFlag(int side) {
    return flags.at(side);
}

glm::vec2 Battlefield::GetEnemyFlag(int side) {
    for(auto p : flags)
    {
        if(p.first != side)
            return p.second;
    }
}

glm::vec2 Battlefield::GetCommanderOfRegiment(int reg, int side) {
    for(auto u : units){
        if(u->component()->IsCommander() && u->component()->GetRegiment() == reg && u->component()->GetSide() == side)
            return glm::vec2(u->Position.x, u->Position.z);
    }
}

bool Battlefield::HasCommanderOfRegiment(int reg, int side) {
    for(auto u : units){
        if(u->component()->IsCommander() && u->component()->GetRegiment() == reg && u->component()->GetSide() == side)
            return true;
    }
    return false;
}

void Battlefield::IssueCommands() {
    for(auto u : units){
        if(u->component()->IsCommander()){
            u->GetBehaviourComponent()->IssueCommands();
        }
    }
}

void Battlefield::IssueOrderToRegiment(glm::vec2 order, int regiment, int side) {
    for(auto u : units)
    {
        if(u->component()->GetRegiment() == regiment && u->component()->GetSide() == side)
            u->GetBehaviourComponent()->Order(order);
    }
}
