
#include <iostream>
#include <algorithm>
#include <glad/glad.h>
#include "GFXengine.h"
#include "ResourceManager.h"

std::map<std::string, std::vector<RenderComponent*>> GFXengine::components;
Camera* GFXengine::activeCamera;
Renderer GFXengine::renderer;
UImanager GFXengine::ui;

void GFXengine::Draw() {
    Shader s = ResourceManager::GetShader("instanced");
    Shader def = ResourceManager::GetShader("default");
    Shader outline = ResourceManager::GetShader("outline");

    glm::mat4 view = activeCamera->GetViewMatrix();
    glm::mat4 projection = activeCamera->GetProjectionMatrix();

    s.use();
    s.setMat4("view", view);
    s.setMat4("projection", projection);
    s.setVec3("viewPos", activeCamera->GetPosition());
    def.use();
    def.setMat4("view",view);
    def.setMat4("projection", projection);
    def.setVec3("viewPos", activeCamera->GetPosition());
    outline.use();
    outline.setMat4("view",view);
    outline.setMat4("projection", projection);

    for(const auto& p : components) {
        renderer.DrawInstanced(p.second, s);
    }

    /// stencil
    for(const auto& p : components)
    {
        for(RenderComponent* rc : p.second)
        {
            if(rc->IsOutlined()){
                /// Draw selected unit(s) again to update stencil
                glStencilFunc(GL_ALWAYS, 1, 0xFF);
                glStencilMask(0xFF);
                renderer.Draw(rc, def, glm::vec3(1.0f));
                /// and again but scaled up and with stencil test
                glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
                glStencilMask(0x00);
                //glDisable(GL_DEPTH_TEST);
                renderer.DrawAltered(rc, outline, rc->GetOutlineColor(),glm::vec3(0.0f,-0.2f,0.0f),0.0f,glm::vec3(1.2f, 1.1f,1.2f));
                //glEnable(GL_DEPTH_TEST);

                glStencilFunc(GL_ALWAYS, 1, 0xFF);
                glStencilMask(0x00);
            }
        }
    }

    ui.Draw();
}

void GFXengine::RegisterComponent(RenderComponent *component) {
    components[component->GetModelName()].push_back(component);
}

void GFXengine::RegisterMainCamera(Camera *cam) {
    activeCamera = cam;
    ui.RegisterMainCamera(cam);
}

void GFXengine::DeleteComponent(RenderComponent *component) {
    std::string modelName = component->GetModelName();
    std::vector<RenderComponent*>& cs = components.at(modelName);
    cs.erase(std::remove_if(std::begin(cs), std::end(cs), [component](RenderComponent* c){return c == component;}), cs.end());
    if(cs.empty()) components.erase(modelName);
}

void GFXengine::AllocateInstanceBuffers() {
    for(const auto& p : components){
        std::vector<glm::mat4> models;
        std::transform(p.second.begin(), p.second.end(), std::back_inserter(models), [](const RenderComponent* c){ return c->GetModelMatrix();});
        ResourceManager::GetModel(p.first).AllocateInstanceBuffers(models);
    }
}

void GFXengine::RegisterUIcomponent(SpriteComponent *component) {
    ui.RegisterComponent(component);
}

void GFXengine::DeleteUIcomponent(SpriteComponent *component) {
    ui.DeleteComponent(component);
}

void GFXengine::Initialize() {
    ui.Init();
}

void GFXengine::RegisterUIcomponent(TextComponent *component) {
    ui.RegisterComponent(component);
}

void GFXengine::DeleteUIcomponent(TextComponent *component) {
    ui.DeleteComponent(component);
}
