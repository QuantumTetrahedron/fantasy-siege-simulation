
#include "Component.h"

CompType Component::GetType() const{
    return type;
}

void Component::SetParent(Object *p) {
    parent = p;
}

Component::Component(Object *p) {
    SetParent(p);
}

const Object *Component::GetParent() const {
    return parent;
}
