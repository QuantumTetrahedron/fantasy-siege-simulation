
#ifndef RAY_H
#define RAY_H

#include <experimental/optional>
#include <vector>
#include "Camera.h"
#include "BoxColliderComponent.h"

class Ray {
public:
    Ray(const Camera& cam, double scrX, double scrY);

    void Collide(const std::vector<BoxColliderComponent*>& colliders);
private:
    glm::vec3 position;
    glm::vec3 direction;
};


#endif
