
#ifndef INPUT_H
#define INPUT_H

#include <glm/glm.hpp>

class Input {
public:

    void PressKey(int key);
    void ReleaseKey(int key);
    bool GetKey(int key);

    void LeftMouseButtonPress();
    void LeftMouseButtonRelease();
    bool MousePressed();

    void SetMousePosition(float x, float y);
    glm::vec2 GetMousePosition();

    void SetScroll(float v);
    float GetScrollOffset();

    void ResetOffsets();
private:
    bool keys[1024];
    bool leftMouseButton;

    glm::vec2 mousePosition;

    float scroll;
};


#endif
