
#include <glad/glad.h>
#include <sstream>
#include <iostream>
#include "Mesh.h"

Mesh::Mesh(std::vector<Vertex> v, std::vector<unsigned int> i, std::vector<Tex> t)
: vertices(v), indices(i), textures(t){
    SetupMesh();
    SetupInstancingData();
}

void Mesh::Draw(const Shader &shader) const {
    SetupDraw(shader);

    shader.use();
    glBindVertexArray(VAO);

    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    glActiveTexture(GL_TEXTURE0);
}

void Mesh::DrawInstanced(const std::vector<glm::mat4>& instanceMats, const Shader &shader, int count) const {
    SetupDraw(shader);

    shader.use();
    glBindVertexArray(VAO);

    UpdateInstanceVBO(instanceMats);

    glDrawElementsInstanced(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0, count);
    glBindVertexArray(0);

    glActiveTexture(GL_TEXTURE0);
}

void Mesh::SetupDraw(const Shader &shader) const {
    shader.use();
    unsigned int diffuseNr = 1;
    for(unsigned int i = 0; i < textures.size(); ++i){
        glActiveTexture(GL_TEXTURE0 + i);
        std::stringstream ss;
        std::string number;
        std::string name = textures[i].type;
        if(name == "texture_diffuse")
            ss << diffuseNr++;
        number = ss.str();
        shader.setInt(name+number, i);
        glBindTexture(GL_TEXTURE_2D, textures[i].id);
    }
}

void Mesh::SetupMesh() {
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,sizeof(Vertex),(void*)(offsetof(Vertex,Normal)));
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,sizeof(Vertex),(void*)(offsetof(Vertex,TexCoords)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0);
}

void Mesh::SetupInstancingData() {
    glGenBuffers(1, &instanceVBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);

    GLsizei vec4size = sizeof(glm::vec4);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4size, (void*)0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4size, (void*)vec4size);
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4size, (void*)(2*vec4size));
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4size, (void*)(3*vec4size));
    glEnableVertexAttribArray(6);

    glVertexAttribDivisor(3, 1);
    glVertexAttribDivisor(4, 1);
    glVertexAttribDivisor(5, 1);
    glVertexAttribDivisor(6, 1);

    glBindVertexArray(0);

}

void Mesh::AlllocateInstanceVBO(const std::vector<glm::mat4> &instanceMats) {
    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
    glBufferData(GL_ARRAY_BUFFER, instanceMats.size() * sizeof(glm::mat4), &instanceMats[0], GL_DYNAMIC_DRAW);
}

void Mesh::UpdateInstanceVBO(const std::vector<glm::mat4> &instanceMats) const {
    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
    glBufferSubData(GL_ARRAY_BUFFER, 0, instanceMats.size() * sizeof(glm::mat4), &instanceMats[0]);
}
