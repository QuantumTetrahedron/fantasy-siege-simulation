
#include <glm/gtc/matrix_transform.hpp>
#include "TextComponent.h"
#include "Object.h"

TextComponent::TextComponent(Object *p, std::string text, float scale, glm::vec4 color)
:Component(p), text(text), fontSize(fontSize), color(color), scale(scale){
    type = CompType::text;
}

glm::mat4 TextComponent::GetModelMatrix() const {
    glm::mat4 model(1.0f);
    model = glm::translate(model, parent->Position);
    model = glm::scale(model, glm::vec3(scale,scale,1.0f));
    return model;
}

glm::vec4 TextComponent::GetColor() const {
    return color;
}

float TextComponent::GetScale() const {
    return scale;
}

std::string TextComponent::GetText() const {
    return text;
}

void TextComponent::SetColor(glm::vec4 newColor) {
    color = newColor;
}

void TextComponent::SetScale(float newScale) {
    scale = newScale;
}

void TextComponent::SetText(std::string newText) {
    text = newText;
}
