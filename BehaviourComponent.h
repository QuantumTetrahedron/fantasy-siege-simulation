
#ifndef BEHAVIOURCOMPONENT_H
#define BEHAVIOURCOMPONENT_H

#include <map>
#include <vector>
#include <random>
#include <glm/glm.hpp>
#include "Component.h"
#include "Personality.h"

class BehaviourComponent : public Component{
public:
    BehaviourComponent(Object* parent, std::string unitName);

    virtual void Update(double dt);

    void IssueCommands();
    void CalculatePrefferedPosition();
    void Move();
    void FindTargets();
    void Attack();

    void Damage(int damageDealt);
    void Order(glm::vec2 newOrder);
    glm::vec2 GetOrder() const;

    bool IsMoving() const;
    int GetMoveRange() const;
    int GetSightRange() const;
    int GetAttackRange() const;
    int GetCommandRange() const;
    bool IsInjured() const;

    int GetHitPoints() const;
    int GetMaxHitPoints() const;
    int GetArmorClass() const;
    int GetCooldown() const;
    int GetCurrentCooldown() const;

    int RollInitiative() const;
    int RollAttack() const;
    int RollDamage() const;

    const std::vector<std::vector<float>>& GetPenalties() const;
    const std::vector<glm::vec2>& GetTargets() const;
    const std::vector<std::pair<glm::vec2, int>>& GetAttacks() const;
    const std::vector<float>& GetCommandReasoning() const;

    int targetPosX;
    int targetPosY;

    bool IsDead() const;
private:
    void LoadPersonalityFromFile(const char* path);

    bool moving;

    bool injured;
    bool dead;

    int moveRange;
    int sightRange;
    int attackRange;
    int commandRange;
    int attackCooldown;
    int toNextAttack;
    int armorClass;
    int hitPoints;
    int maxHitPoints;

    glm::vec2 receivedOrder;
    bool gotOrder;

    Personality personality;

    std::vector<std::vector<float>> penalties_v;
    std::vector<glm::vec2> attackTargets;
    std::vector<std::pair<glm::vec2, int>> attacksPerformed;
    std::vector<float> commandReasoning;
    float enemyFlagForCommanderAdvance;

    static std::random_device rd;
    static std::default_random_engine generator;
};

#endif
