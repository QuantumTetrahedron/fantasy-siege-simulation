
#ifndef UNIT_H
#define UNIT_H

#include "Object.h"
#include "UnitComponent.h"
#include "RenderComponent.h"
#include "BoxColliderComponent.h"
#include "BehaviourComponent.h"
#include <string>

class Unit : public Object{
public:
    class Factory{
    public:
        static Unit* Spawn(int x,int y, std::string name);
        static Unit* SpawnCommander(int x, int y, std::string name);
        static void SetParameter(std::string name, std::string value);
    private:
        static void SetRace(UnitComponent::Race r);
        static void SetArchetype(UnitComponent::Archetype a);
        static void SetRegiment(int reg);
        static void SetSide(int s);

        static UnitComponent::Race race;
        static UnitComponent::Archetype archetype;
        static int regiment;
        static int side;
    };

    UnitComponent* component() const;
    BehaviourComponent* GetBehaviourComponent() const;

    void Select();
    void Deselect();

    void Move();
    bool IsMoving() const;

    void Damage(int damageDealt);
    bool IsDead() const;

    void CleanUp();
private:
    Unit(std::string name, int x, int y, UnitComponent::Archetype a, UnitComponent::Race r, int s, int reg, bool isCommander);
    std::string GetUnitTypeName(UnitComponent::Archetype archetype, UnitComponent::Race race, bool isCommander);
    UnitComponent* unitComponent;
    RenderComponent* renderComponent;
    BehaviourComponent* behaviourComponent;
};


#endif
