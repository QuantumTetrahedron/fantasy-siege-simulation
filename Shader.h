
#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <glm/glm.hpp>

class Shader {
public:
    Shader(){}
    Shader(const char* vertexFilePath, const char* fragmentFilePath);

    void use() const;

    void setBool(const std::string& name, bool value) const;
    void setInt(const std::string &name, int value) const;
    void setFloat(const std::string &name, float value) const;
    void setVec2(const std::string &name, const glm::vec2 &value) const;
    void setVec2(const std::string &name, float x, float y) const;
    void setVec3(const std::string &name, const glm::vec3 &value) const;
    void setVec3(const std::string &name, float x, float y, float z) const;
    void setVec4(const std::string &name, const glm::vec4 &value) const;
    void setVec4(const std::string &name, float x, float y, float z, float w) const;
    void setMat4(const std::string &name, const glm::mat4 &mat) const;

private:
    void LoadFromFile(const char* vertexFilePath, const char* fragmentFilePath);
    void checkErrors(unsigned int shader, std::string type);

    unsigned int id;
};


#endif
