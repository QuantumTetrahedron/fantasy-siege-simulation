
#include <iostream>
#include "Text.h"
#include "GFXengine.h"

Text::Text(glm::vec2 screenSize, std::string text, glm::vec2 pos, float scale, glm::vec4 color, UIelement *parent)
: UIelement(screenSize, pos, parent){
    textComponent = new TextComponent(this, text, scale, color);
    AddComponent(textComponent);
    GFXengine::RegisterUIcomponent(textComponent);
}

void Text::Scale(float s) {
    textComponent->SetScale(s);
}

float Text::GetScale() {
    return textComponent->GetScale();
}

void Text::SetText(std::string text) {
    textComponent->SetText(text);
}
