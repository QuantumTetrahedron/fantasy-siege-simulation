
#ifndef WINDOW_H
#define WINDOW_H


#include <GLFW/glfw3.h>
#include <functional>

class Window {
public:
    Window(unsigned int width, unsigned int height, const char* title);

    void makeCurrentContext() const;

    void SetKeyCallback(void (*callback)(GLFWwindow *window, int key, int scancode, int action, int mods));
    void SetMouseMoveCallback(void (*callback)(GLFWwindow *window, double xpos, double ypos));
    void SetScrollCallback(void (*callback)(GLFWwindow *window, double xoffset, double yoffset));
    void SetMouseButtonCallback(void (*callback)(GLFWwindow *window, int button, int action, int mods));

    GLFWwindow* get() const;
private:
    GLFWwindow* window;
};


#endif
