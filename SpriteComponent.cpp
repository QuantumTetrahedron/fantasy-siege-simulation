
#include <glm/gtc/matrix_transform.hpp>
#include "SpriteComponent.h"
#include "Object.h"

SpriteComponent::SpriteComponent(Object *p, Texture tex, glm::vec2 size, glm::vec4 color, float rotation)
: Component(p), texture(tex), size(size), color(color), space(Space::_2D), rotation(rotation), pitch(0.0f){
    type = CompType::sprite;
}

SpriteComponent::SpriteComponent(Object *p, Texture tex, glm::vec2 size, glm::vec4 color, float yaw, float pitch)
: Component(p), texture(tex), size(size), color(color), space(Space::_3D), rotation(yaw), pitch(pitch){
    type = CompType::sprite;
}

glm::mat4 SpriteComponent::GetModelMatrix() {
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, parent->Position);
    if(space == Space::_3D) {
        model = glm::rotate(model, glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(pitch), glm::vec3(1.0f, 0.0f, 0.0f));
    }
    else
        model = glm::rotate(model, glm::radians(rotation), glm::vec3(0.0f,0.0f,1.0f));
    model = glm::translate(model, glm::vec3(-0.5f * size.x, -0.5f * size.y, 0.0f));
    model = glm::scale(model, glm::vec3(size, 1.0f));
    return model;
}

glm::vec4 SpriteComponent::GetColor() const {
    return color;
}

Texture SpriteComponent::GetTexture() const {
    return texture;
}

bool SpriteComponent::Is3D() const {
    return space == Space::_3D;
}

void SpriteComponent::SetColor(glm::vec4 newColor) {
    color = newColor;
}

glm::vec2 SpriteComponent::GetSize() {
    return size;
}

void SpriteComponent::SetSize(glm::vec2 newSize) {
    size = newSize;
}