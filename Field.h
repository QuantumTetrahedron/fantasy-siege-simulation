
#ifndef FIELD_H
#define FIELD_H


#include "Object.h"

class Field : public Object{
public:
    enum class TerrainType{
        empty, wall, water, unknown
    };

    Field(int x, int y, TerrainType t);

    bool isTrespassable() const;
private:
    TerrainType terrainType;
    void Initialize();
};


#endif
