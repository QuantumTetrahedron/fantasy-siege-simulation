
#include "UIelement.h"

UIelement::UIelement(glm::vec2 screenSize, glm::vec2 pos, UIelement *p)
: Object(glm::vec3(0.0f)), screenSize(screenSize){
    parent = p;
    if(parent) parent->AddChild(this);
    SetPos(pos);
}

void UIelement::Update(double dt) {
    Object::Update(dt);

    glm::vec2 targetPos = GetTargetPos();

    glm::vec2 dir = targetPos - GetPos();
    if(glm::length(dir) > dt * 600.0f){
        dir = glm::normalize(dir) * (float)dt * 600.0f;
        MoveTo(GetPos() + dir);
    }
    else
        SetPos(targetPosition);
}

void UIelement::SetPos(glm::vec2 position) {
    MoveTo(position);
    SetTargetPos(position);
}

void UIelement::SetTargetPos(glm::vec2 targetPos) {
    targetPosition = targetPos;
}

void UIelement::AddChild(UIelement *child) {
    children.push_back(child);
}

glm::vec2 UIelement::GetPos() {
    if(parent)
        return glm::vec2(Position) /* screenSize/2.0f*/ - parent->GetScreenPos();
    else
        return glm::vec2(Position);// * screenSize/2.0f;
}

glm::vec2 UIelement::GetScreenPos() {
    return glm::vec2(Position);
}

glm::vec2 UIelement::GetTargetPos() {
    glm::vec2 targetPos = targetPosition;
    if(parent)
        targetPos += parent->GetTargetPos() - parent->GetPos();
    return targetPos;
}

void UIelement::MoveTo(glm::vec2 position) {
    if(parent)
        Position = glm::vec3((parent->GetScreenPos() + position), 0.0f);
    else
        Position = glm::vec3(position, 0.0f);
}

