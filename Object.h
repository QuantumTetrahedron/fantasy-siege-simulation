#ifndef OBJECT_H
#define OBJECT_H

#include <glm/glm.hpp>

#include <vector>
#include "Component.h"

class Object {
public:
    Object(int x, int y);
    Object(glm::vec3 pos);
    Object(const Object& other) = delete;
    Object(Object&& other);
    Object& operator=(const Object& other) = delete;
    Object& operator=(Object&& other);
    virtual ~Object();

    glm::vec3 Position;

    void AddComponent(Component* c);
    Component* GetComponent(CompType type) const;

    virtual void Update(double dt);
private:
    std::vector<Component*> components;
};


#endif
