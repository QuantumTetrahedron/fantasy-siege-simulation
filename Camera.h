
#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>

class Camera {
public:
    enum Movement {
        Forward,Backward,Left,Right
    };

    Camera(unsigned int width, unsigned int height, glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f));

    glm::mat4 GetViewMatrix() const;
    glm::mat4 GetProjectionMatrix() const;

    void ProcessKeyboard(Movement direction, double deltaTime);

    void ProcessMouseMovement(float xoffset, float yoffset);

    void ProcessMouseScroll(float scrollOffset);

    glm::vec3 GetPosition() const;
    glm::vec2 GetScreenSize() const;
private:
    void updateCameraVectors();

    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;
    glm::vec3 worldUp;
    glm::vec3 worldFront;

    float yaw;
    float pitch;

    glm::vec3 Anchor;
    float height;

    float movementSpeed;
    float mouseSensitivity;

    const float zoom;
    int screenWidth;
    int screenHeight;
};


#endif
