
#include "BoxColliderComponent.h"
#include "Object.h"

BoxColliderComponent::BoxColliderComponent(Object *parent, glm::vec3 objectSize, std::function<void()> collisionCallback)
: Component(parent), size(objectSize), OnCollision(collisionCallback){}

bool BoxColliderComponent::Collided(glm::vec3 point) const {
    glm::vec3 p = parent->Position;
    return Between(point.x, p.x - size.x/2.0f ,p.x + size.x/2.0f)
            && Between(point.y, p.y-size.y/2.0f, p.y+size.y/2.0f)
            && Between(point.z, p.z-size.z/2.0f, p.z+size.z/2.0f);
}

bool BoxColliderComponent::Between(double v, double l, double r) const {
    return l < r ? v >= l && v <= r : v >= r && v <= l;
}

void BoxColliderComponent::CheckCollision(glm::vec3 point) const {
    if(Collided(point)) OnCollision();
}
