
#include <queue>
#include <set>
#include <iostream>
#include <algorithm>
#include "Pathfinder.h"
#include "Battlefield.h"

std::map<glm::vec2, std::map<glm::vec2, float, Pathfinder::vec2comparator>, Pathfinder::vec2comparator> Pathfinder::distances;


void Pathfinder::LoadMap(const std::vector<Field *> &fields) {
    std::queue<glm::vec2> toCheck;
    for(Field* f : fields){
        float fx = f->Position.x;
        float fy = f->Position.z;
        std::cout << "generating paths from: " << (int)fx/2 << " " << (int)fy/2 << std::endl;
        std::map<glm::vec2, float, vec2comparator> distancesFromF;
        int currDist = 0;
        int stepCount = 1;
        toCheck.push(glm::vec2(fx,fy));

        std::set<glm::vec2, vec2comparator> alreadyAdded;
        alreadyAdded.insert(glm::vec2(fx,fy));
        while(!toCheck.empty()){
            glm::vec2 checking = toCheck.front();
            toCheck.pop();
            distancesFromF[checking] = currDist;

            if(Battlefield::TerrainIsTrespassable(checking.x, checking.y)) {
                for (int i = -2; i <= 2; i += 2) {
                    for (int j = -2; j <= 2; j += 2) {
                        glm::vec2 v(checking.x + i, checking.y + j);
                        if(alreadyAdded.find(v) == alreadyAdded.end()){
                            alreadyAdded.insert(v);
                            toCheck.push(v);
                        }
                    }
                }
            }
            stepCount -= 1;
            if(stepCount == 0){
                currDist++;
                stepCount = toCheck.size();
            }
        }
        glm::vec2 asd = glm::vec2(fx,fy);
        std::pair<glm::vec2, std::map<glm::vec2,float,vec2comparator>> p(asd,distancesFromF);
        distances.emplace(p);
    }
}

float Pathfinder::GetDistance(glm::vec2 from, glm::vec2 to) {
    float ret = -1.0f;
    try {
        ret = distances.at(from).at(to);
    }catch (...){}
    return ret;
}
