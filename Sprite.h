
#ifndef SPRITE_H
#define SPRITE_H

#include <string>
#include "SpriteComponent.h"
#include "UIelement.h"

class Sprite : public UIelement{
public:
    Sprite();
    Sprite(glm::vec2 screenSize, std::string textureName, glm::vec2 pos, glm::vec2 size, glm::vec4 color, float rotation, UIelement* parent = nullptr);

    void SetSize(glm::vec2 newSize);
    glm::vec2 GetSize() const;

    void SetColor(glm::vec4 color);
private:
    SpriteComponent* uIcomponent;
};


#endif
