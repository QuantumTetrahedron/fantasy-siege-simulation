
#include "UnitComponent.h"

UnitComponent::UnitComponent(Object* p, std::string name, UnitComponent::Archetype a, UnitComponent::Race r, int s, int reg, bool isCommander)
: Component(p), isCommander(isCommander), name(name){
    type = CompType::unit;
    setArchetype(a);
    setRace(r);
    setSide(s);
    setRegiment(reg);
}

UnitComponent::Archetype UnitComponent::GetArchetype() const {
    return archetype;
}

UnitComponent::Race UnitComponent::GetRace() const {
    return race;
}

int UnitComponent::GetRegiment() const {
    return regimment;
}

int UnitComponent::GetSide() const {
    return side;
}

void UnitComponent::setArchetype(UnitComponent::Archetype a) {
    archetype = a;
}

void UnitComponent::setRace(UnitComponent::Race r) {
    race = r;
}

void UnitComponent::setRegiment(int reg) {
    regimment = reg;
}

void UnitComponent::setSide(int s) {
    side = s;
}

bool UnitComponent::IsCommander() const {
    return isCommander;
}

std::string UnitComponent::GetName() const {
    return name;
}
