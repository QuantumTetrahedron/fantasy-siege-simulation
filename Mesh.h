
#ifndef MESH_H
#define MESH_H

#include <assimp/Importer.hpp>
#include <glm/glm.hpp>
#include <vector>
#include "Shader.h"

struct Vertex{
    glm::vec3 Position = glm::vec3(0.0f);
    glm::vec3 Normal = glm::vec3(0.0f);
    glm::vec2 TexCoords = glm::vec2(0.0f);
};

struct Tex{
    unsigned int id;
    std::string type;
    aiString path;
};

class Mesh {
public:
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Tex> textures;

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Tex> textures);

    void Draw(const Shader& shader) const;
    void DrawInstanced(const std::vector<glm::mat4>& instanceMats, const Shader& shader, int count) const;
    void AlllocateInstanceVBO(const std::vector<glm::mat4>& instanceMats);
    void UpdateInstanceVBO(const std::vector<glm::mat4>& instanceMats) const;
private:
    unsigned int VAO, VBO, EBO;
    unsigned int instanceVBO;
    void SetupDraw(const Shader& shader) const;
    void SetupMesh();
    void SetupInstancingData();
};


#endif
