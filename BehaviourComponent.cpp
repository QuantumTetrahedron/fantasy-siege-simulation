
#include <iostream>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <sstream>
#include "BehaviourComponent.h"
#include "Object.h"
#include "Battlefield.h"
#include "Pathfinder.h"

std::random_device BehaviourComponent::rd;
std::default_random_engine BehaviourComponent::generator(rd());

void BehaviourComponent::Update(double dt) {
    // TODO: Set y pos from field altitude
    glm::vec3 dir = glm::vec3(targetPosX, 0,targetPosY) - parent->Position;

    if(moving)
    {
        float speed = dt * 6.0f;
        if(glm::length(dir) > speed){
            parent->Position += glm::normalize(dir) * speed;
        }
        else{
            parent->Position = glm::vec3(targetPosX, 0, targetPosY);
            moving = false;
        }
    }
}

void BehaviourComponent::CalculatePrefferedPosition() {
    for(int i=0;i<penalties_v.size();++i)
        penalties_v[i].clear();
    penalties_v.clear();

    UnitComponent* unitComponent = dynamic_cast<UnitComponent*>(parent->GetComponent(CompType::unit));

    int aliveFriendlyAgentsCount = 0;
    int aliveEnemyAgentsCount = 0;
    int injuredFriendlyAgentsCount = 0;
    int injuredEnemyAgentCount = 0;

    for(int si = -sightRange; si <= sightRange; ++si)
    {
        for(int sj = -sightRange; sj <= sightRange; ++ sj)
        {
            if(si == 0 && sj == 0) continue;

            int x = static_cast<int>(glm::round(parent->Position.x + 2*si));
            int y = static_cast<int>(glm::round(parent->Position.z + 2*sj));
            const Unit* u = Battlefield::getUnit(x,y);
            if(u)
            {
                if(u->component()->GetSide() == unitComponent->GetSide()) {
                    if(u->GetBehaviourComponent()->IsInjured())
                        injuredFriendlyAgentsCount++;
                    else
                        aliveFriendlyAgentsCount++;
                }
                else{
                    if(u->GetBehaviourComponent()->IsInjured())
                        injuredEnemyAgentCount++;
                    else
                        aliveEnemyAgentsCount++;
                }
            }
        }
    }
    aliveFriendlyAgentsCount = glm::max(aliveFriendlyAgentsCount, 1);
    aliveEnemyAgentsCount = glm::max(aliveEnemyAgentsCount, 1);
    injuredFriendlyAgentsCount = glm::max(injuredFriendlyAgentsCount, 1);
    injuredEnemyAgentCount = glm::max(injuredEnemyAgentCount, 1);

    float currentDistanceToFriendlyFlag = Pathfinder::GetDistance(glm::vec2(parent->Position.x, parent->Position.z), Battlefield::GetFriendlyFlag(unitComponent->GetSide()));
    float currentDistanceToEnemyFlag = Pathfinder::GetDistance(glm::vec2(parent->Position.x, parent->Position.z), Battlefield::GetEnemyFlag(unitComponent->GetSide()));
    currentDistanceToFriendlyFlag = glm::max(1.0f,currentDistanceToFriendlyFlag);
    currentDistanceToEnemyFlag = glm::max(1.0f,currentDistanceToEnemyFlag);

    bool hasCommander = Battlefield::HasCommanderOfRegiment(unitComponent->GetRegiment(), unitComponent->GetSide());
    bool shouldObeyCommand = hasCommander && !unitComponent->IsCommander() && gotOrder;

    glm::vec2 commanderPos = glm::vec2(-1.0f);
    float currentDistanceToLC = -1.0f;
    float currentDistanceToTarget = -1.0f;
    if(shouldObeyCommand) {
        commanderPos = Battlefield::GetCommanderOfRegiment(unitComponent->GetRegiment(), unitComponent->GetSide());
        currentDistanceToLC = Pathfinder::GetDistance(glm::vec2(parent->Position.x, parent->Position.z), commanderPos);
        currentDistanceToTarget = Pathfinder::GetDistance(glm::vec2(parent->Position.x, parent->Position.z), receivedOrder);
        currentDistanceToTarget = glm::max(1.0f,currentDistanceToTarget);
    }

    for(int i = -moveRange; i <= moveRange; ++i)
    {
        penalties_v.push_back(std::vector<float>());

        for(int j = -moveRange; j <= moveRange; ++j){
            if(Pathfinder::GetDistance(glm::vec2(parent->Position.x, parent->Position.z), glm::vec2(parent->Position.x + 2*i, parent->Position.z + 2*j)) > moveRange)
            {
                penalties_v[i+moveRange].push_back(INT_MAX);
                continue;
            }
            float aliveFriendsDistanceSum = 0;
            float injuredFriendsDistanceSum = 0;
            float aliveEnemiesDistanceSum = 0;
            float injuredEnemyDistanceSum = 0;

            bool obstackle = false;
            for(int si = -sightRange; si <= sightRange; ++si){

                for(int sj = -sightRange; sj <= sightRange; ++sj){

                    int x = static_cast<int>(glm::round(parent->Position.x + 2*si));
                    int y = static_cast<int>(glm::round(parent->Position.z + 2*sj));
                    const Unit* unit = Battlefield::getUnit(x,y);
                    if(unit && unit != parent)
                    {
                        ///float distX = glm::abs(unit->Position.x - (parent->Position.x + 2*i));
                        ///float distY = glm::abs(unit->Position.z - (parent->Position.z + 2*j));
                        ///float dist = glm::max(distX, distY);
                        ///float dist = glm::sqrt(distX*distX + distY*distY);
                        float dist = Pathfinder::GetDistance(glm::vec2(parent->Position.x + 2*i, parent->Position.z + 2*j),glm::vec2(unit->Position.x, unit->Position.z));
                        if(dist == -1)
                        {
                            obstackle = true;
                            break;
                        }
                        if(unit->component()->GetSide() == unitComponent->GetSide()){
                            aliveFriendsDistanceSum += dist;
                        }
                        else
                            aliveEnemiesDistanceSum += dist;
                    }
                }
                if(obstackle)break;
            }

            glm::vec2 checkedPos = glm::vec2(parent->Position.x + 2*i, parent->Position.z + 2*j);
            float newDistanceToFriendlyFlag = Pathfinder::GetDistance(checkedPos, Battlefield::GetFriendlyFlag(unitComponent->GetSide()));
            float newDistanceToEnemyFlag = Pathfinder::GetDistance(checkedPos, Battlefield::GetEnemyFlag(unitComponent->GetSide()));
            if(newDistanceToFriendlyFlag == -1 || newDistanceToEnemyFlag == -1) obstackle = true;

            float newDistanceToLC = -1;
            float newDistanceToTarget = -1;
            if(shouldObeyCommand){
                newDistanceToLC = Pathfinder::GetDistance(checkedPos, commanderPos);
                newDistanceToTarget = Pathfinder::GetDistance(checkedPos, receivedOrder);
            }

            float penalty = INT_MAX;
            if(!obstackle)
            {
                penalty = (1.0f / glm::sqrt(2.0f * sightRange))
                          * (personality.Get("aliveFriends") * aliveFriendsDistanceSum / aliveFriendlyAgentsCount
                          + personality.Get("injuredFriends") * injuredFriendsDistanceSum / injuredFriendlyAgentsCount)
                          + (1.0f / glm::sqrt(2.0f * sightRange))
                          * (personality.Get("aliveEnemies") * aliveEnemiesDistanceSum / aliveEnemyAgentsCount
                          + personality.Get("injuredEnemies") * injuredEnemyDistanceSum / injuredEnemyAgentCount)
                          + personality.Get("friendlyFlag") * newDistanceToFriendlyFlag / currentDistanceToFriendlyFlag
                          + personality.Get("enemyFlag") * newDistanceToEnemyFlag / currentDistanceToEnemyFlag;

                if(currentDistanceToLC > 4)
                    penalty += personality.Get("localCommander") * newDistanceToLC / currentDistanceToLC;
                if(shouldObeyCommand){
                    penalty += personality.Get("localCommanderObey") * newDistanceToTarget / currentDistanceToTarget;
                }
            }


            penalties_v[i+moveRange].push_back(penalty);
        }
    }
    gotOrder = false;
}

BehaviourComponent::BehaviourComponent(Object* p, std::string unitName) : Component(p){
    dead = false;
    injured = false;
    moving = false;
    gotOrder = false;
    type = CompType::behaviour;
    enemyFlagForCommanderAdvance = 0;

    targetPosX = parent->Position.x;
    targetPosY = parent->Position.z;
    toNextAttack = 0;

    LoadPersonalityFromFile(("UnitTypes/"+unitName+".unit").c_str());
}

int BehaviourComponent::GetMoveRange() const {
    return moveRange;
}

void BehaviourComponent::Move() {
    std::vector<std::pair<float, glm::vec2>> v;
    for(int i = 0;i<penalties_v.size();++i) {
        for (int j = 0;j<penalties_v[i].size();++j) {
            v.emplace_back(penalties_v[i][j], glm::vec2(i,j));
        }
    }
    std::sort(v.begin(), v.end(), [](auto a, auto b){
        return a.first < b.first;
    });

    std::vector<glm::vec2> t;
    bool topFound = false;
    for(int i=0;i<v.size();++i){
        if(topFound && v[i].first > v[i-1].first)
            break;

        int x = parent->Position.x + (v[i].second.x - moveRange) * 2.0f;
        int y = parent->Position.z + (v[i].second.y - moveRange) * 2.0f;
        const Unit* u = Battlefield::getUnit(x,y);
        if((!u || u == parent) && Battlefield::TerrainIsTrespassable(x,y)) {
            t.emplace_back(x,y);
            topFound = true;
        }
    }
    std::shuffle(t.begin(),t.end(), generator);
    targetPosX = t[0].x;
    targetPosY = t[0].y;
    moving = true;
}

void BehaviourComponent::LoadPersonalityFromFile(const char *path) {
    std::string line;
    std::ifstream file(path);
    while(std::getline(file, line)){
        std::string param;
        float value;

        std::istringstream stream(line);
        stream >> param >> value;
        if(param == "speed")
            moveRange = value;
        else if(param == "sight")
            sightRange = value;
        else if(param == "range")
            attackRange = value;
        else if(param == "cooldown")
            attackCooldown = value;
        else if(param == "ac")
            armorClass = value;
        else if(param == "hp")
            maxHitPoints = hitPoints = value;
        else if(param == "commandRange")
            commandRange = value;
        else
            personality.Set(param, value);
    }
    personality.Normalize();
    enemyFlagForCommanderAdvance = personality.Get("enemyFlag");
    file.close();
}

bool BehaviourComponent::IsMoving() const {
    return moving;
}

const std::vector<std::vector<float>> &BehaviourComponent::GetPenalties() const {
    return penalties_v;
}

int BehaviourComponent::GetSightRange() const {
    return sightRange;
}

int BehaviourComponent::GetAttackRange() const {
    return attackRange;
}

void BehaviourComponent::FindTargets() {
    if(!attackTargets.empty())attackTargets.clear();
    UnitComponent* unitComponent = dynamic_cast<UnitComponent*>(parent->GetComponent(CompType::unit));
    for(int i = -attackRange; i<=attackRange; ++i){
        for(int j = -attackRange; j<=attackRange; ++j){
            int x = static_cast<int>(glm::round(parent->Position.x + 2*i));
            int y = static_cast<int>(glm::round(parent->Position.z + 2*j));
            const Unit* u = Battlefield::getUnit(x,y);
            if(u)
            {
                if(u->component()->GetSide() != unitComponent->GetSide())
                    attackTargets.push_back(glm::vec2(x,y));
            }
        }
    }
}

const std::vector<glm::vec2> &BehaviourComponent::GetTargets() const{
    return attackTargets;
}

void BehaviourComponent::Attack() {
    if(!attacksPerformed.empty())
        attacksPerformed.clear();
    if(dead) return;

    if(toNextAttack == 0){
        if(attackTargets.empty()) return;
        std::shuffle(attackTargets.begin(), attackTargets.end(), generator);

        glm::ivec2 pos = attackTargets[0];
        Unit* unit = Battlefield::getUnit(pos.x, pos.y);
        /// roll d20
        int attackRoll = RollAttack();
        ///compare to AC
        int ac = unit->GetBehaviourComponent()->GetArmorClass();
        if(attackRoll >= ac){
            /// roll damage
            int damageRoll = RollDamage();
            unit->Damage(damageRoll);
            attacksPerformed.emplace_back(pos,1);
        }
        else attacksPerformed.emplace_back(pos,0);

        for(int i = 1; i < attackTargets.size(); ++i){
            attacksPerformed.emplace_back(attackTargets[i], -1);
        }

        toNextAttack = attackCooldown;
    }
    else{
        toNextAttack--;
    }
}

int BehaviourComponent::GetArmorClass() const {
    return armorClass;
}

void BehaviourComponent::Damage(int damageDealt) {
    hitPoints -= damageDealt;
    injured = true;
    if(hitPoints <= 0)
        dead = true;
}

const std::vector<std::pair<glm::vec2, int>> &BehaviourComponent::GetAttacks() const {
    return attacksPerformed;
}

bool BehaviourComponent::IsDead() const {
    return dead;
}

int BehaviourComponent::RollInitiative() const {
    std::uniform_int_distribution<> d20(1,20);
    return d20(generator);
}

int BehaviourComponent::RollAttack() const {
    std::uniform_int_distribution<> d20(1,20);
    return d20(generator);
}

int BehaviourComponent::RollDamage() const {
    std::uniform_int_distribution<> d4(1,4);
    return d4(generator);
}

bool BehaviourComponent::IsInjured() const {
    return injured;
}

void BehaviourComponent::IssueCommands() {
    if(!commandReasoning.empty())commandReasoning.clear();

    UnitComponent* unitComponent = dynamic_cast<UnitComponent*>(parent->GetComponent(CompType::unit));

    int enemiesNearby = 0;

    glm::vec2 order;
    float maxProp = -1;

    for(int i = -1 ; i <= 1; ++i){
        for(int j = -1; j <= 1; ++j){
            glm::vec2 commandZonePos = glm::vec2(parent->Position.x + 2*i * commandRange, parent->Position.z + 2*j * commandRange);
            int subZoneRadius = (commandRange - 1) / 2;
            int enemiesInSubzone = 0;
            int friendsInSubzone = 0;
            for(int zi = -subZoneRadius; zi <= subZoneRadius; ++zi){
                for(int zj = -subZoneRadius; zj <=subZoneRadius; ++zj) {
                    Unit *u = Battlefield::getUnit(commandZonePos.x + 2*zi, commandZonePos.y + 2*zj);
                    if (u) {
                        if(u->component()->GetSide() == unitComponent->GetSide())
                            friendsInSubzone++;
                        else {
                            enemiesInSubzone++;
                            float dist = Pathfinder::GetDistance(glm::vec2(parent->Position.x, parent->Position.z), glm::vec2(commandZonePos.x + 2*zi, commandZonePos.y + 2*zj));
                            if(dist != -1.0f && dist <= commandRange+1)
                                enemiesNearby++;
                        }
                    }
                }
            }
            float unitProportions = (float)enemiesInSubzone / glm::max(friendsInSubzone,1);
            commandReasoning.push_back(unitProportions);

            if(unitProportions > maxProp){
                maxProp = unitProportions;
                order = commandZonePos;
            }
            else if(unitProportions == maxProp){
                float oldCommandToEnemyFlagDistance = Pathfinder::GetDistance(order, Battlefield::GetEnemyFlag(unitComponent->GetSide()));
                if(oldCommandToEnemyFlagDistance == -1) oldCommandToEnemyFlagDistance = INT_MAX;
                float newCommandToEnemyFlagDistance = Pathfinder::GetDistance(commandZonePos, Battlefield::GetEnemyFlag(unitComponent->GetSide()));
                if(newCommandToEnemyFlagDistance == -1) newCommandToEnemyFlagDistance = INT_MAX;

                if(newCommandToEnemyFlagDistance < oldCommandToEnemyFlagDistance){
                    order = commandZonePos;
                }
            }
        }
    }
    if(enemiesNearby > 0){
        personality.Set("enemyFlag", 5);

        Battlefield::IssueOrderToRegiment(order, unitComponent->GetRegiment(), unitComponent->GetSide());
    }
    else {
        personality.Set("enemyFlag", enemyFlagForCommanderAdvance);
    }
}

void BehaviourComponent::Order(glm::vec2 newOrder) {
    gotOrder = true;
    receivedOrder = newOrder;
}

int BehaviourComponent::GetCommandRange() const {
    UnitComponent* unitComponent = dynamic_cast<UnitComponent*>(parent->GetComponent(CompType::unit));
    if(unitComponent->IsCommander())
        return commandRange;
    else return 0;
}

const std::vector<float> &BehaviourComponent::GetCommandReasoning() const {
    return commandReasoning;
}

glm::vec2 BehaviourComponent::GetOrder() const {
    if(gotOrder)
        return receivedOrder;
    else return glm::vec2(-1.0f);
}

int BehaviourComponent::GetHitPoints() const {
    return hitPoints;
}

int BehaviourComponent::GetMaxHitPoints() const {
    return maxHitPoints;
}

int BehaviourComponent::GetCooldown() const {
    return attackCooldown;
}

int BehaviourComponent::GetCurrentCooldown() const {
    return toNextAttack;
}
