
#ifndef SPRITERENDERER_H
#define SPRITERENDERER_H


#include <glm/glm.hpp>
#include "Texture.h"
#include "Shader.h"
#include "SpriteComponent.h"

class SpriteRenderer {
public:
    void DrawSprite(const Shader& shader, SpriteComponent* uic) const;
    void initRenderData();
private:
    unsigned int vao;
};

#endif
