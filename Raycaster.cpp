
#include "Raycaster.h"

Camera* Raycaster::camera;
std::vector<BoxColliderComponent*> Raycaster::registeredColliders;

Ray Raycaster::Spawn(double x, double y) {
    return Ray(*camera, x, y);
}

void Raycaster::RegisterCamera(Camera *cam) {
    camera = cam;
}

void Raycaster::RegisterCollider(BoxColliderComponent *collider) {
    registeredColliders.push_back(collider);
}

void Raycaster::ProcessCollision(Ray ray) {
    ray.Collide(registeredColliders);
}
