
#include "ResourceManager.h"

std::map<std::string, Shader> ResourceManager::shaders;
std::map<std::string, Model> ResourceManager::models;
std::map<std::string, Texture> ResourceManager::textures;

Shader ResourceManager::GetShader(std::string name) {
    return shaders.at(name);
}

void
ResourceManager::LoadShader(const char *vertexShaderFilePath, const char *fragmentShaderFilePath, std::string name) {
    if(shaders.find(name) != shaders.end())
        return;
    shaders.emplace(std::pair<std::string, Shader>(name, Shader(vertexShaderFilePath, fragmentShaderFilePath)));
}

void ResourceManager::DeleteShader(std::string name) {
    std::map<std::string, Shader>::iterator it;
    it = shaders.find(name);
    if(it != shaders.end())
        shaders.erase(it);
}

Model ResourceManager::GetModel(std::string name) {
    return models.at(name);
}

void ResourceManager::LoadModel(const char *filePath, std::string name) {
    if(models.find(name) != models.end())
        return;
    models.emplace(std::pair<std::string, Model>(name, Model(filePath)));
}

void ResourceManager::DeleteModel(std::string name) {
    std::map<std::string, Model>::iterator it;
    it = models.find(name);
    if(it != models.end());
        models.erase(it);
}

Texture ResourceManager::GetTexture(std::string name) {
    return textures.at(name);
}

void ResourceManager::LoadTexture(const char *filePath, bool alpha, std::string name) {
    if(textures.find(name) != textures.end())
        return;
    textures.emplace(std::pair<std::string, Texture>(name, Texture(filePath, alpha)));
}

void ResourceManager::DeleteTexture(std::string name) {
    auto it = textures.find(name);
    if(it != textures.end())
        textures.erase(it);
}
