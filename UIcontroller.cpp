
#include <iostream>
#include "UIcontroller.h"
#include "BehaviourComponent.h"
#include "ResourceManager.h"
#include "Text.h"

Grid UIcontroller::grid;
Unit* UIcontroller::selectedUnit;
std::map<std::string, UIelement*> UIcontroller::elements;
Battlefield::State UIcontroller::currentState;

void UIcontroller::SetSelection(Unit *newUnit) {
    if(selectedUnit) {
        Deselect();
    }

    selectedUnit = newUnit;
    selectedUnit->Select();
    elements.at("dataPanel")->SetTargetPos(elements.at("dataPanel")->GetTargetPos()+glm::vec2(420.0f, 0.0f));
    UpdateDataPanel();

    UpdateGrid(selectedUnit);
}

void UIcontroller::Init(Camera* cam) {
    glm::vec2 screenSize = cam->GetScreenSize();
    elements["bar"] = new Sprite(screenSize,"blank", glm::vec2(512.0f, 718.0f), glm::vec2(1024.0f,100.0f), glm::vec4(80.0f/256.0f,51.0f/256.0f,22.0f/256.0f,1.0f), 0.0f);
    elements["slider"] = new Sprite(screenSize,"blank", glm::vec2(512.0f-300.0f, 718.0f), glm::vec2(110.0f,110.0f), glm::vec4(183.0f/256.0f,108.0f/256.0f,18.0f/256.0f,1.0f), 0.0f);

    elements["iconLC"] = new Sprite(screenSize, "flag", glm::vec2(512.0f-300.0f,718.0f), glm::vec2(100.0f, 100.0f), glm::vec4(1.0f,1.0f,0.0f,1.0f),0.0f);
    elements["iconPF"] = new Sprite(screenSize,"pathfinding", glm::vec2(512.0f-150.0f,718.0f), glm::vec2(100.0f, 100.0f), glm::vec4(1.0f,1.0f,0.0f,1.0f), 0.0f);
    elements["iconMV"] = new Sprite(screenSize,"moving", glm::vec2(512.0f,718.0f), glm::vec2(100.0f, 100.0f), glm::vec4(1.0f,1.0f,0.0f,1.0f), 0.0f);
    elements["iconFT"] = new Sprite(screenSize,"aiming", glm::vec2(512.0f+150.0f,718.0f), glm::vec2(100.0f, 100.0f), glm::vec4(1.0f,1.0f,0.0f,1.0f), 0.0f);
    elements["iconAT"] = new Sprite(screenSize,"attacking", glm::vec2(512.0f+300.0f,718.0f), glm::vec2(100.0f, 100.0f), glm::vec4(1.0f,1.0f,0.0f,1.0f), 0.0f);

    elements["dataPanel"] = new Sprite(screenSize,"blank", glm::vec2(-200.0f, 768.0f/2.0f-50.0f), glm::vec2(400.0f, 650.0f), glm::vec4(80.0f/256.0f,51.0f/256.0f,22.0f/256.0f,1.0f), 0.0f);

    elements["Character name"] = new Text(screenSize, "Character name", glm::vec2(-190.0f,-300.0f), 0.75f, glm::vec4(0.3f,0.0f,0.0f,1.0f), elements.at("dataPanel"));
    elements["Character type"] = new Text(screenSize, "Type", glm::vec2(-175.0f,-250.0f),0.5f,glm::vec4(0.0f,0.0f,0.0f,1.0f),elements.at("dataPanel"));
    elements["Character hp"] = new Text(screenSize, "Hit Points : ", glm::vec2(-175.0f, -200.0f), 0.5f, glm::vec4(0.0f,0.0f,0.0f,1.0f), elements.at("dataPanel"));
    elements["Max HP bar"] = new Sprite(screenSize, "blank", glm::vec2(0.0f,-160.0f), glm::vec2(380.0f, 20.0f), glm::vec4(0.5f,0.5f,0.5f,1.0f),0.0f, elements.at("dataPanel"));
    elements["Current HP bar"] = new Sprite(screenSize, "blank", glm::vec2(-95.0f,0.0f), glm::vec2(190.0f, 20.0f), glm::vec4(0.4f,0.1f,0.1f,1.0f),0.0f, elements.at("Max HP bar"));
    elements["Character ac"] = new Text(screenSize, "Armor Class : ", glm::vec2(-175.0f, -140.0f), 0.5f, glm::vec4(0.0f,0.0f,0.0f,1.0f), elements.at("dataPanel"));

    elements["Character cd"] = new Text(screenSize, "Attack cooldown : ", glm::vec2(-175.0f,-100.0f),0.5f,glm::vec4(0.0f,0.0f,0.0f,1.0f),elements.at("dataPanel"));
    elements["Max CD bar"] = new Sprite(screenSize, "blank", glm::vec2(0.0f,-60.0f), glm::vec2(380.0f, 20.0f), glm::vec4(0.5f,0.5f,0.5f,1.0f),0.0f, elements.at("dataPanel"));
    elements["Current CD bar"] = new Sprite(screenSize, "blank", glm::vec2(-95.0f,0.0f), glm::vec2(190.0f, 20.0f), glm::vec4(0.4f,0.1f,0.1f,1.0f),0.0f, elements.at("Max CD bar"));

    currentState = Battlefield::State::Start;
}

void UIcontroller::Deselect() {
    if(selectedUnit){
        selectedUnit->Deselect();
        elements.at("dataPanel")->SetTargetPos(elements.at("dataPanel")->GetTargetPos()+glm::vec2(-420.0f,0.0f));
        selectedUnit = nullptr;
        grid.Clear();
    }
}

void UIcontroller::SetState(Battlefield::State state) {
    if(currentState != state) {
        currentState = state;
        Sprite* LCsprite = dynamic_cast<Sprite*>(elements.at("iconLC"));
        Sprite* pathfindingSprite = dynamic_cast<Sprite*>(elements.at("iconPF"));
        Sprite* movingSprite = dynamic_cast<Sprite*>(elements.at("iconMV"));
        Sprite* findingSprite = dynamic_cast<Sprite*>(elements.at("iconFT"));
        Sprite* attackingSprite = dynamic_cast<Sprite*>(elements.at("iconAT"));

        pathfindingSprite->SetColor(glm::vec4(1.0f, 1.0f, 0.0f,1.0f));
        movingSprite->SetColor(glm::vec4(1.0f, 1.0f, 0.0f,1.0f));
        findingSprite->SetColor(glm::vec4(1.0f, 1.0f, 0.0f,1.0f));
        attackingSprite->SetColor(glm::vec4(1.0f, 1.0f, 0.0f,1.0f));
        LCsprite->SetColor(glm::vec4(1.0f,1.0f,0.0f,1.0f));

        if(state == Battlefield::State::CommandPhase){
            elements.at("slider")->SetPos(LCsprite->GetPos());
            LCsprite->SetColor(glm::vec4(1.0f,0.0f,0.0f,1.0f));
        }
        else if(state == Battlefield::State::CommandPhaseEnded){
            elements.at("slider")->SetPos(LCsprite->GetPos());
            LCsprite->SetColor(glm::vec4(0.0f,1.0f,0.0f,1.0f));
        }
        else if (state == Battlefield::State::CalculatingMoves) {
            elements.at("slider")->SetPos(pathfindingSprite->GetPos());
            pathfindingSprite->SetColor(glm::vec4(1.0f, 0.0f, 0.0f,1.0f));
        }
        else if(state == Battlefield::State::CalculatingMovesEnded){
            elements.at("slider")->SetPos(pathfindingSprite->GetPos());
            pathfindingSprite->SetColor(glm::vec4(0.0f, 1.0f, 0.0f,1.0f));
        }
        else if(state == Battlefield::State::Moving){
            elements.at("slider")->SetPos(movingSprite->GetPos());
            movingSprite->SetColor(glm::vec4(1.0f, 0.0f, 0.0f,1.0f));
        }
        else if(state == Battlefield::State::MovingEnded){
            elements.at("slider")->SetPos(movingSprite->GetPos());
            movingSprite->SetColor(glm::vec4(0.0f, 1.0f, 0.0f,1.0f));
        }
        else if(state == Battlefield::State::FindingTargets){
            elements.at("slider")->SetPos(findingSprite->GetPos());
            findingSprite->SetColor(glm::vec4(1.0f, 0.0f, 0.0f,1.0f));
        }
        else if(state == Battlefield::State::FindingTargetsEnded){
            elements.at("slider")->SetPos(findingSprite->GetPos());
            findingSprite->SetColor(glm::vec4(0.0f, 1.0f, 0.0f,1.0f));
        }
        else if(state == Battlefield::State::Attacking){
            elements.at("slider")->SetPos(attackingSprite->GetPos());
            attackingSprite->SetColor(glm::vec4(1.0f, 0.0f, 0.0f,1.0f));
        }
        else if(state == Battlefield::State::AttackingEnded){
            elements.at("slider")->SetPos(attackingSprite->GetPos());
            attackingSprite->SetColor(glm::vec4(0.0f, 1.0f, 0.0f,1.0f));
            if(selectedUnit){
                if(selectedUnit->IsDead())
                    Deselect();
                else
                    UpdateDataPanel();
            }
        }
        UpdateGrid(selectedUnit);
    }
}

std::vector<std::vector<glm::vec4>> UIcontroller::ColorsFromPenalties(const std::vector<std::vector<float>> &penalties) {
    std::vector<std::vector<glm::vec4>> ret;

    float max = INT_MIN;
    float min = INT_MAX;
    for(const std::vector<float>& pv : penalties) {
        for (float p : pv) {
            if(p > 1000) continue;
            max = glm::max(max, p);
            min = glm::min(min, p);
        }
    }

    for(const std::vector<float>& pv : penalties){
        std::vector<glm::vec4> r;
        for(float p : pv){
            if(p > 1000)
                r.push_back(glm::vec4(0.0f,0.0f,0.0f,0.2f));
            else if(p == min)
                r.push_back(glm::vec4(1.0f,0.0f,0.0f,1.0f));
            else
                r.push_back(glm::vec4(0.0f, 1-(p-min)/(max-min), 0.0f,1.0f));
        }
        ret.push_back(r);
    }

    return ret;
}

std::vector<std::vector<glm::vec4>> UIcontroller::ColorsFromPath(const std::vector<glm::vec2> &path) {
    std::vector<std::vector<glm::vec4>> ret;

    return ret;
}

std::vector<std::vector<glm::vec4>> UIcontroller::ColorsFromTargets(const std::vector<glm::vec2> &targets) {
    std::vector<std::vector<glm::vec4>> ret;
    int r = grid.GetRadius();
    for(int i = -r; i <= r; ++i){
        std::vector<glm::vec4> v;
        for(int j = -r; j <= r; ++j){
            glm::vec2 pos = glm::vec2(grid.GetPos().x + 2*i, grid.GetPos().z + 2*j);
            if(std::find(targets.begin(), targets.end(), pos) != targets.end()){
                v.push_back(glm::vec4(1.0f,0.0f,0.0f,1.0f));
            }
            else{
                v.push_back(glm::vec4(0.0f,0.0f,0.0f,0.2f));
            }
        }
        ret.push_back(v);
    }

    return ret;
}

std::vector<std::vector<glm::vec4>> UIcontroller::ColorsFromAttacks(const std::vector<std::pair<glm::vec2, int>> attacks) {
    std::vector<std::vector<glm::vec4>> ret;

    int r = grid.GetRadius();
    for(int i = -r; i <= r; ++i){
        std::vector<glm::vec4> v;
        for(int j = -r; j <= r; ++j){
            glm::vec2 pos = glm::vec2(grid.GetPos().x + 2*i, grid.GetPos().z + 2*j);
            auto it = std::find_if(attacks.begin(), attacks.end(), [pos](auto p){return p.first == pos;});
            if(it != attacks.end()){
                if(it->second == 1) // hit
                    v.push_back(glm::vec4(1.0f,0.0f,0.0f,1.0f));
                else if(it->second == 0)// miss
                    v.push_back(glm::vec4(0.0f,0.0f,1.0f,1.0f));
                else // did not attack
                    v.push_back(glm::vec4(0.0f,1.0f,0.0f,0.5f));
            }
            else{
                v.push_back(glm::vec4(0.0f,0.0f,0.0f,0.2f));
            }
        }
        ret.push_back(v);
    }

    return ret;
}

void UIcontroller::Update(double dt) {
    for(auto p : elements)
        p.second->Update(dt);
    if(selectedUnit)
        grid.SetPos(selectedUnit->Position);
}

void UIcontroller::UpdateGrid(Unit* u) {
    if(u) {
        BehaviourComponent *bc = u->GetBehaviourComponent();
        if(currentState == Battlefield::State::CommandPhaseEnded){
            if(u->component()->IsCommander()){
                grid.Generate(u->Position, 1, bc->GetCommandRange());
                grid.SetColors(ColorsFromCommandZone(bc->GetCommandReasoning()));
            }
            else {
                glm::vec2 order = bc->GetOrder();
                if (order.x != -1){
                    grid.Generate(glm::vec3(bc->GetOrder().x, 0.0f, bc->GetOrder().y), 1);
                    grid.SetColors(ColorsFromCommand());
                }
            }
        }
        else if (currentState == Battlefield::State::CalculatingMovesEnded) {
            grid.Generate(u->Position, bc->GetMoveRange());
            grid.SetColors(ColorsFromPenalties(bc->GetPenalties()));
        } else if (currentState == Battlefield::State::FindingTargetsEnded) {
            grid.Generate(u->Position, bc->GetAttackRange());
            grid.SetColors(ColorsFromTargets(bc->GetTargets()));
        } else if (currentState == Battlefield::State::AttackingEnded) {
            grid.Generate(u->Position, bc->GetAttackRange());
            grid.SetColors((ColorsFromAttacks(bc->GetAttacks())));
        }
    }
}

std::vector<std::vector<glm::vec4>> UIcontroller::ColorsFromCommandZone(const std::vector<float> &commandReasoning) {
    std::vector<std::vector<glm::vec4>> ret;

    float max = 1;
    for(float i : commandReasoning)
        max = glm::max(max, i);

    int it = 0;
    for(int i = -1;i<=1;++i){
        std::vector<glm::vec4> vec;
        for(int j = -1;j<=1;++j){
            float v = commandReasoning[it];

            glm::vec4 color(0.0f, v / max, 0.0f,1.0f);

            vec.push_back(color);

            it++;
        }
        ret.push_back(vec);
    }

    return ret;
}

std::vector<std::vector<glm::vec4>> UIcontroller::ColorsFromCommand() {
    return std::vector<std::vector<glm::vec4>>(3,std::vector<glm::vec4>(3,glm::vec4(1.0f,0.0f,0.0f,1.0f)));
}

std::string UIcontroller::RaceToString(UnitComponent::Race race) {
    if(race == UnitComponent::Race::elf) return "Elf";
    if(race == UnitComponent::Race::human) return "Human";
    if(race == UnitComponent::Race::orc) return "Orc";
    return "unknown";
}

std::string UIcontroller::ArchetypeToString(UnitComponent::Archetype arch) {
    if(arch == UnitComponent::Archetype::commander) return "Commander";
    if(arch == UnitComponent::Archetype::soldier) return "Soldier";
    if(arch == UnitComponent::Archetype::ranger) return "Ranger";
    return "unknown";
}

void UIcontroller::UpdateDataPanel() {

    Text* t = dynamic_cast<Text*>(elements.at("Character name"));
    if(t) t->SetText(selectedUnit->component()->GetName());

    t = dynamic_cast<Text*>(elements.at("Character type"));
    std::string type = RaceToString(selectedUnit->component()->GetRace())
                       + (selectedUnit->component()->IsCommander() ? " Commander " : " ")
                       + ArchetypeToString(selectedUnit->component()->GetArchetype());
    t->SetText(type);

    int maxHP = selectedUnit->GetBehaviourComponent()->GetMaxHitPoints();
    int HP = selectedUnit->GetBehaviourComponent()->GetHitPoints();
    t = dynamic_cast<Text*>(elements.at("Character hp"));
    std::string hpText = "Hit Points : ";
    hpText += std::to_string(HP) + " / " + std::to_string(maxHP);
    t->SetText(hpText);
    Sprite* HPbar = dynamic_cast<Sprite*>(elements.at("Current HP bar"));
    float hpPercent = (float)HP / maxHP;
    HPbar->SetPos(glm::vec2(-190.0f + 190.0f * hpPercent, 0.0f));
    HPbar->SetSize(glm::vec2(380.0f*hpPercent,20.0f));

    int ac = selectedUnit->GetBehaviourComponent()->GetArmorClass();
    t = dynamic_cast<Text*>(elements.at("Character ac"));
    t->SetText("Armor class : " + std::to_string(ac));

    int maxcd = selectedUnit->GetBehaviourComponent()->GetCooldown();
    int cd = selectedUnit->GetBehaviourComponent()->GetCurrentCooldown();
    cd = maxcd - cd;
    float cdp = (float)cd/maxcd;
    Sprite* CDbar = dynamic_cast<Sprite*>(elements.at("Current CD bar"));
    CDbar->SetPos(glm::vec2(-190.0f+190.0f*cdp,0.0f));
    CDbar->SetSize(glm::vec2(380.0f * cdp, 20.0f));
    t = dynamic_cast<Text*>(elements.at("Character cd"));
    t->SetText("Attack cooldown : " + std::to_string(cd) + " / " + std::to_string(maxcd));
}

