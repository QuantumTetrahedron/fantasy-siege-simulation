
#include "Window.h"

Window::Window(unsigned int width, unsigned int height, const char *title) {
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    window = glfwCreateWindow(width, height, title, nullptr, nullptr);
}

GLFWwindow *Window::get() const {
    return window;
}

void Window::makeCurrentContext() const {
    glfwMakeContextCurrent(window);
}

void Window::SetKeyCallback(void (* callback)(GLFWwindow *window, int key, int scancode, int action, int mode)) {
    glfwSetKeyCallback(window, callback);
}

void Window::SetMouseMoveCallback(void (*callback)(GLFWwindow *, double, double)) {
    glfwSetCursorPosCallback(window, callback);
}

void Window::SetScrollCallback(void (*callback)(GLFWwindow *, double, double)) {
    glfwSetScrollCallback(window, callback);
}

void Window::SetMouseButtonCallback(void (*callback)(GLFWwindow *, int, int, int)) {
    glfwSetMouseButtonCallback(window, callback);
}
