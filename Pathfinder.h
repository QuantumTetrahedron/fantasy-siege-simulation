
#ifndef PATHFINDER_H
#define PATHFINDER_H

#include <glm/glm.hpp>
#include <map>
#include "Field.h"

class Pathfinder {
public:
    static void LoadMap(const std::vector<Field*>& fields);
    static float GetDistance(glm::vec2 from, glm::vec2 to);

    struct vec2comparator{
        bool operator()(const glm::vec2& a, const glm::vec2& b){
            return a.x < b.x ? true : a.x == b.x && a.y < b.y;
        }
    };
private:
    static std::map<glm::vec2, std::map<glm::vec2, float, vec2comparator>, vec2comparator> distances;

};

#endif
