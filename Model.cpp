
#include <iostream>
#include <glad/glad.h>
#include "Model.h"
#include "stb_image.h"

Model::Model(const std::string &path){
    loadModel(path);
}

void Model::Draw(const Shader &shader) const {
    for(const Mesh& mesh : meshes)
        mesh.Draw(shader);
}

void Model::DrawInstanced(const std::vector<glm::mat4>& instanceMats, const Shader &shader, int count) const {
    for(const Mesh& mesh : meshes)
        mesh.DrawInstanced(instanceMats, shader, count);
}

void Model::loadModel(const std::string &path) {
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

    if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cerr << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return;
    }

    directory = path.substr(0, path.find_last_of('/'));
    processNode(scene->mRootNode, scene);

}

void Model::processNode(aiNode *node, const aiScene *scene) {
    for(unsigned int i = 0; i < node->mNumMeshes; ++i)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh, scene));
    }
    for(unsigned int i = 0; i < node->mNumChildren; ++i)
        processNode(node->mChildren[i], scene);
}

Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene) {
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Tex> textures;

    for(unsigned int i = 0; i < mesh->mNumVertices; ++i)
    {
        Vertex vertex;
        glm::vec3 vector;

        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;

        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.Normal = vector;

        if(mesh->mTextureCoords[0]){
            glm::vec2 vec;
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        }
        else
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);

        vertices.push_back(vertex);
    }

    for(unsigned int i = 0; i < mesh->mNumFaces; ++i)
    {
        aiFace face = mesh->mFaces[i];
        for(unsigned int j = 0; j < face.mNumIndices; ++j){
            indices.push_back(face.mIndices[j]);
        }
    }

    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

    std::vector<Tex> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse", false);
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

    return Mesh(vertices, indices, textures);
}

std::vector<Tex> Model::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName, bool gammaCorrection) {
    std::vector<Tex> textures;

    for(unsigned int i=0; i< mat->GetTextureCount(type); ++i){
        aiString str;
        mat->GetTexture(type, i, &str);
        bool skip = false;
        for(unsigned int j = 0; j< texturesLoaded.size(); ++j){
            if(std::strcmp(texturesLoaded[j].path.C_Str(), str.C_Str()) == 0){
                textures.push_back(texturesLoaded[j]);
                skip = true;
                break;
            }
        }
        if(!skip){
            Tex texture;
            texture.id = TextureFromFile(str.C_Str(), directory, gammaCorrection);
            texture.type = typeName;
            texture.path = str;
            textures.push_back(texture);
            texturesLoaded.push_back(texture);
        }
    }
    return textures;
}

unsigned int Model::TextureFromFile(const char *path, const std::string &directory, bool gammaCorrection) {
    std::string filename = directory + "/" + path;

    unsigned int texID;
    glGenTextures(1,&texID);

    int width, height, nrChannels;
    unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrChannels, 0);
    if(data){
        GLenum internalFormat;
        GLenum dataFormat;
        if(nrChannels == 1)
            internalFormat = dataFormat = GL_RED;
        else if(nrChannels == 3){
            internalFormat = gammaCorrection ? GL_SRGB : GL_RGB;
            dataFormat = GL_RGB;
        }
        else if(nrChannels == 4){
            internalFormat = gammaCorrection? GL_SRGB_ALPHA : GL_RGBA;
            dataFormat = GL_RGBA;
        }
        else return 0;
        glBindTexture(GL_TEXTURE_2D, texID);
        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else std::cout << "Texture failed to load: " << path << std::endl;

    stbi_image_free(data);
    return texID;
}

void Model::AllocateInstanceBuffers(const std::vector<glm::mat4>& matrices) {
    for(auto& m : meshes)
        m.AlllocateInstanceVBO(matrices);
}
