
#include <algorithm>
#include "Object.h"

void Object::AddComponent(Component* c) {
    if(std::find(components.begin(), components.end(), c)!=components.end())
        return;
    components.push_back(c);
}

Component *Object::GetComponent(CompType type) const {
    for(Component* c: components){
        if(c->GetType() == type)
            return c;
    }
    return nullptr;
}

void Object::Update(double dt) {
    for(Component* c: components){
        c->Update(dt);
    }
}

Object::Object(int x, int y)
        : Position(x,0,y){}

Object::Object(glm::vec3 pos)
        : Position(pos){}

Object::Object(Object &&other) {
    Position = other.Position;
    components = other.components;
}

Object &Object::operator=(Object &&other){
    Position = other.Position;
    components = other.components;
    return *this;
}

Object::~Object() {
    for(Component* c : components){
        delete c;
        c = nullptr;
    }
    components.clear();
}
