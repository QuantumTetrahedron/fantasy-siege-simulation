
#include <glad/glad.h>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include "SpriteRenderer.h"
#include "Object.h"

void SpriteRenderer::DrawSprite(const Shader& shader, SpriteComponent* uic) const {
    shader.use();
    glm::mat4 model = uic->GetModelMatrix();

    shader.setMat4("model", model);

    shader.setVec4("spriteColor", uic->GetColor());
    shader.setBool("is3d", uic->Is3D());

    shader.setInt("texture_diffuse1", 0);

    glActiveTexture(GL_TEXTURE0);
    uic->GetTexture().Bind();

    if(!uic->Is3D())
        glDisable(GL_DEPTH_TEST);
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glEnable(GL_DEPTH_TEST);
}

void SpriteRenderer::initRenderData() {
    unsigned int VBO;
    float vertices[] = {
            0.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f
    };

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &VBO);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,2*sizeof(float),0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
