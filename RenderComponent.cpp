
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include "RenderComponent.h"
#include "Object.h"

RenderComponent::RenderComponent(Object* parent, std::string modelName, RenderComponent::RenderType type)
: Component(parent), modelName(modelName), renderType(type){
    this->type = CompType::render;
    outlined = false;
}

glm::mat4 RenderComponent::GetModelMatrix() const {
    glm::mat4 mat(1.0f);
    mat = glm::translate(mat, parent->Position);
    return mat;
}

RenderComponent::RenderType RenderComponent::GetRenderType() const {
    return renderType;
}

std::string RenderComponent::GetModelName() const {
    return modelName;
}

bool RenderComponent::IsOutlined() const {
    return outlined;
}

void RenderComponent::SetOutline(bool o, glm::vec3 color) {
    outlined = o;
    outlineColor = color;
}

glm::vec3 RenderComponent::GetOutlineColor() {
    return outlineColor;
}
