
#include <iostream>
#include "Field.h"
#include "RenderComponent.h"
#include "GFXengine.h"

Field::Field(int x, int y, Field::TerrainType t)
: Object(x,y), terrainType(t){
    Initialize();
}

void Field::Initialize() {
    std::string modelName;
    glm::vec3 color;
    if(terrainType == TerrainType::empty){
        modelName = "field";
        color = glm::vec3( 0.2f, 0.8f, 0.2f);
    }
    else if(terrainType == TerrainType::water) {
        modelName = "water";
        color = glm::vec3( 0.0f, 0.0f, 0.5f);
    }
    else if(terrainType == TerrainType::wall) {
        modelName = "wall";
        color = glm::vec3(0.2f);
    }
    else {
        modelName = "null";
        color = glm::vec3(1.0f);
    }
    RenderComponent* component = new RenderComponent(this, modelName, RenderComponent::RenderType::instanced);
    AddComponent(component);
    GFXengine::RegisterComponent(component);
}

bool Field::isTrespassable() const {
    return terrainType == TerrainType::empty;
}
