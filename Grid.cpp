
#include <iostream>
#include "Grid.h"
#include "ResourceManager.h"
#include "GFXengine.h"

Grid::Grid() : size(0){}

void Grid::Generate(glm::vec3 pos, int r, int scale) {
    Clear();
    size = 2*r+1;
    position = pos;
    for(int i = 0;i<size;++i){
        quads.push_back(std::vector<Quad>());
        quads[i].reserve(size);
        for(int j = 0;j<size;++j){
            int relativeX = i - (size-1)/2;
            int relativeY = j - (size-1)/2;
            quads[i].emplace_back(position + glm::vec3(2.0f*relativeX * scale, 0.1f, 2.0f*relativeY * scale), glm::vec2(2 * scale,2 * scale), glm::vec4((float)i / size, (float)j / size, 0.0f,1.0f));
        }
    }
}

void Grid::SetPos(glm::vec3 newPos) {
    position = newPos;
}

void Grid::Clear() {
    if(quads.empty()) return;

    for(int i = 0;i<size;++i) {
        for (int j = 0; j < size; ++j) {
            quads[i][j].Clear();
        }
        quads[i].clear();
    }
    quads.clear();
}

void Grid::SetColors(const std::vector<std::vector<glm::vec4>> &colors) {
    if(colors.size()!=size)return;
    for(int i = 0;i<size;++i){
        if(colors[i].size() != size) return;}

    for(int i = 0;i<size;++i){
        for(int j = 0;j<size;++j){
            quads[i][j].SetColor(colors[i][j]);
        }
    }
}

int Grid::GetRadius() const {
    return (size-1)/2;
}

glm::vec3 Grid::GetPos() const {
    return position;
}

Quad::Quad(glm::vec3 position, glm::vec2 size, glm::vec4 color)
: Object(position){
    Texture tex = ResourceManager::GetTexture("blank");
    uic = new SpriteComponent(this, tex, size, color, 0.0f, 90.0f);
    AddComponent(uic);
    GFXengine::RegisterUIcomponent(uic);
}

void Quad::SetColor(glm::vec4 color) {
    uic->SetColor(color);
}

void Quad::Clear() {
    GFXengine::DeleteUIcomponent(uic);
}
