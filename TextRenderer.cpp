
#include <glm/gtc/matrix_transform.hpp>
#include <glad/glad.h>
#include "TextRenderer.h"
#include "ResourceManager.h"
#include <iostream>
#include "Object.h"

#include <ft2build.h>
#include FT_FREETYPE_H

void TextRenderer::Init() {
    textShader = ResourceManager::GetShader("text");
    textShader.use();
    textShader.setInt("text",0);

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float)*6*4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,4,GL_FLOAT,GL_FALSE,4*sizeof(float),0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);

    Load("fonts/font.ttf", 48);
}

void TextRenderer::Load(std::string font, unsigned int fontSize) {
    Characters.clear();
    FT_Library ft;
    if(FT_Init_FreeType(&ft)){
        std::cerr << "Could not init FreeType" << std::endl;
    }
    FT_Face face;
    if(FT_New_Face(ft, font.c_str(), 0, &face)){
        std::cerr << "Failed to load font" << std::endl;
    }

    FT_Set_Pixel_Sizes(face,0,fontSize);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    for(unsigned char c = 0; c < 128; ++c){
        if(FT_Load_Char(face, c, FT_LOAD_RENDER)){
            std::cerr << "Failed to load glyph: " << c << std::endl;
            continue;
        }
        unsigned int tex;
        glGenTextures(1,&tex);
        glBindTexture(GL_TEXTURE_2D, tex);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        Character character = {
                tex,
                glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                face->glyph->advance.x
        };
        Characters.insert(std::pair<char, Character>(c,character));
    }
    glBindTexture(GL_TEXTURE_2D,0);
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
}

void TextRenderer::RenderText(std::string text, float x, float y, float scale, glm::vec4 color) {
    textShader.use();
    textShader.setVec4("textColor",color);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(VAO);
    glDisable(GL_DEPTH_TEST);

    std::string::const_iterator c;
    for(c = text.begin(); c != text.end(); ++c){
        Character ch = Characters[*c];
        float xPos = x+ch.Bearing.x*scale;
        float yPos = y+(Characters['H'].Bearing.y-ch.Bearing.y)*scale;
        float w = ch.Size.x * scale;
        float h = ch.Size.y * scale;

        float vertices[6][4] = {
                {xPos,     yPos + h, 0.0f, 1.0f},
                {xPos + w, yPos,     1.0f, 0.0f},
                {xPos,     yPos,     0.0f, 0.0f},

                {xPos,     yPos + h, 0.0f, 1.0f},
                {xPos + w, yPos + h, 1.0f, 1.0f},
                {xPos + w, yPos,     1.0f, 0.0f}
        };

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, ch.TextureID);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        x += (ch.Advance >> 6) * scale;
    }
    glEnable(GL_DEPTH_TEST);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D,0);
}

void TextRenderer::RenderText(TextComponent *textComponent) {
    glm::vec3 pos = textComponent->GetParent()->Position;
    RenderText(textComponent->GetText(), pos.x, pos.y, textComponent->GetScale(), textComponent->GetColor());
}

