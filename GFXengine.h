
#ifndef GFXENGINE_H
#define GFXENGINE_H

#include <vector>
#include <map>
#include "RenderComponent.h"
#include "Camera.h"
#include "Renderer.h"
#include "SpriteComponent.h"
#include "UImanager.h"
#include "TextComponent.h"

class GFXengine {
public:
    static void Initialize();

    static void Draw();

    static void RegisterMainCamera(Camera* cam);
    static void RegisterComponent(RenderComponent* component);
    static void DeleteComponent(RenderComponent* component);

    static void RegisterUIcomponent(SpriteComponent* component);
    static void RegisterUIcomponent(TextComponent* component);
    static void DeleteUIcomponent(SpriteComponent* component);
    static void DeleteUIcomponent(TextComponent* component);

    static void AllocateInstanceBuffers();
private:
    static Camera* activeCamera;
    static Renderer renderer;

    static std::map<std::string, std::vector<RenderComponent*>> components;

    static UImanager ui;
};


#endif
