
#ifndef GRID_H
#define GRID_H

#include <glm/glm.hpp>
#include <array>
#include "SpriteRenderer.h"
#include "Object.h"
#include "SpriteComponent.h"

class Quad : public Object{
public:
    Quad(glm::vec3 position, glm::vec2 size, glm::vec4 color);
    void SetColor(glm::vec4 color);
    void Clear();
private:
    SpriteComponent* uic;
};

class Grid {
public:
    Grid();
    void Generate(glm::vec3 pos, int r, int scale = 1);
    void SetColors(const std::vector<std::vector<glm::vec4>>& colors);

    void SetPos(glm::vec3 newPos);
    void Clear();

    int GetRadius() const;
    glm::vec3 GetPos() const;
private:
    glm::vec3 position;

    int size;
    std::vector<std::vector<Quad>> quads;
};


#endif
