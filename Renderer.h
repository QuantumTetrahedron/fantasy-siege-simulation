
#ifndef RENDERER_H
#define RENDERER_H

#include <vector>
#include "RenderComponent.h"
#include "Shader.h"

class Renderer {
public:
    void Draw(const RenderComponent* renderComponent, const Shader& shader, glm::vec3 color) const;
    void Draw(const std::vector<RenderComponent*>& components, const Shader& shader) const;
    void DrawAltered(const RenderComponent* renderComponent, const Shader& shader, glm::vec3 color, glm::vec3 posOffset, float rotOffset, glm::vec3 scale);
    void DrawInstanced(const std::vector<RenderComponent*>& components, const Shader& shader) const;
};


#endif
