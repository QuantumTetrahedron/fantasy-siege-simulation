
#include "Input.h"

void Input::PressKey(int key) {
    keys[key] = true;
}

void Input::ReleaseKey(int key) {
    keys[key] = false;
}

bool Input::GetKey(int key) {
    return keys[key];
}

void Input::SetMousePosition(float x, float y) {
    mousePosition = glm::vec2(x,y);
}

glm::vec2 Input::GetMousePosition() {
    return mousePosition;
}

void Input::SetScroll(float v) {
    scroll += v;
}

float Input::GetScrollOffset() {
    return scroll;
}

void Input::ResetOffsets() {
    scroll = 0.0f;
}

void Input::LeftMouseButtonPress() {
    leftMouseButton = true;
}

void Input::LeftMouseButtonRelease() {
    leftMouseButton = false;
}

bool Input::MousePressed() {
    return leftMouseButton;
}
