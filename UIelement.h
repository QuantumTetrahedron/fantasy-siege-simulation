
#ifndef UIELEMENT_H
#define UIELEMENT_H

#include <vector>
#include "Object.h"
#include "Camera.h"

class UIelement : public Object{
public:
    UIelement(glm::vec2 screenSize, glm::vec2 pos, UIelement* p = nullptr);

    virtual void Update(double dt) override;

    void SetPos(glm::vec2 position);
    glm::vec2 GetPos();
    glm::vec2 GetTargetPos();
    glm::vec2 GetScreenPos();
    void SetTargetPos(glm::vec2 targetPos);
    void AddChild(UIelement* child);
protected:
    void MoveTo(glm::vec2 position);

    glm::vec2 targetPosition;
    UIelement* parent;
    std::vector<UIelement*> children;
    glm::vec2 screenSize;
};

#endif
