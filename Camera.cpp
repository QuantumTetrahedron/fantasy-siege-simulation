
#include "Camera.h"

#include <glm/gtc/matrix_transform.hpp>

Camera::Camera(unsigned int width, unsigned int height, glm::vec3 position, glm::vec3 up)
: screenWidth(width), screenHeight(height), worldUp(up), zoom(45.0f), mouseSensitivity(0.25f){
    Anchor = position;
    position = glm::vec3(Anchor.x, 10.0f, Anchor.z + 8.0f);
    yaw = -90.0f;
    pitch = -30.0f;
    movementSpeed = 0.6f * position.y;
    this->height = position.y;
    updateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix() const {
    return glm::lookAt(position, Anchor + glm::vec3(0.0f, 0.0f, -2.0), up);
}

glm::mat4 Camera::GetProjectionMatrix() const {
    return glm::perspective(glm::radians(zoom), (float)screenWidth/(float)screenHeight, 0.1f, 1000.0f);
}

void Camera::ProcessKeyboard(Movement direction, double deltaTime) {
    float velocity = movementSpeed * deltaTime;
    if(direction == Movement::Forward)
        Anchor += worldFront * velocity;
    else if(direction == Movement::Backward)
        Anchor -= worldFront * velocity;
    else if(direction == Movement::Left)
        Anchor -= right * velocity;
    else if(direction == Movement::Right)
        Anchor += right * velocity;
    position = glm::vec3(Anchor.x, height, Anchor.z + 8.0f);
}

void Camera::ProcessMouseMovement(float xoffset, float yoffset) {
    xoffset *= mouseSensitivity;
    yoffset *= mouseSensitivity;

    yaw += xoffset;
    pitch += yoffset;

    if(pitch > -40.0f)
        pitch = -40.0f;
    if(pitch < -50.0f)
        pitch = -50.0f;

    updateCameraVectors();
}

void Camera::updateCameraVectors() {
    glm::vec3 Front;
    Front.x = (float)(cos(glm::radians(yaw)) * cos(glm::radians(pitch)));
    Front.y = (float)sin(glm::radians(pitch));
    Front.z = (float)(sin(glm::radians(yaw)) * cos(glm::radians(pitch)));
    front = glm::normalize(Front);

    right = glm::normalize(glm::cross(front, worldUp));
    up = glm::normalize(glm::cross(right, front));
    worldFront = glm::normalize(glm::cross(worldUp, right));
}

glm::vec3 Camera::GetPosition() const {
    return position;
}

void Camera::ProcessMouseScroll(float scrollOffset) {
    height -= 0.1 * position.y * scrollOffset;
    height = glm::round(height);
    if(height < 5.0f) height = 5.0f;
    if(height > 400.0f) height = 400.0f;
    position = glm::vec3(Anchor.x, height, Anchor.z + 8.0f);
    movementSpeed = position.y;

    updateCameraVectors();
}

glm::vec2 Camera::GetScreenSize() const {
    return glm::vec2(screenWidth, screenHeight);
}
