
#include <sstream>
#include <iostream>
#include "Unit.h"
#include "RenderComponent.h"
#include "GFXengine.h"
#include "BehaviourComponent.h"
#include "Raycaster.h"
#include "UIcontroller.h"

std::string Unit::GetUnitTypeName(UnitComponent::Archetype archetype, UnitComponent::Race race, bool isCommander) {
    std::string name = "";
    if(race == UnitComponent::Race::human)
        name += "human_";
    else if(race == UnitComponent::Race::orc)
        name += "orc_";

    if(isCommander)
        name += "commander";
    else if(archetype == UnitComponent::Archetype::soldier)
        name += "soldier";

    return name;
}

Unit::Unit(std::string name, int x, int y,UnitComponent::Archetype a, UnitComponent::Race r, int s, int reg, bool isCommander)
: Object(x,y){
    unitComponent = new UnitComponent(this,name, a,r,s,reg,isCommander);
    AddComponent(unitComponent);

    std::string modelName = GetUnitTypeName(a, r, isCommander);

    behaviourComponent = new BehaviourComponent(this,modelName);
    AddComponent(behaviourComponent);

    renderComponent = new RenderComponent(this,modelName, RenderComponent::RenderType::instanced);
    AddComponent(renderComponent);
    GFXengine::RegisterComponent(renderComponent);

    BoxColliderComponent* boxCollider = new BoxColliderComponent(this, glm::vec3(2.0f, 6.0f, 2.0f), [this](){
        UIcontroller::SetSelection(this);
    });
    Raycaster::RegisterCollider(boxCollider);
}

UnitComponent *Unit::component() const{
    return unitComponent;
}

void Unit::Select() {
    renderComponent->SetOutline(true, glm::vec3(1.0f,0.0f,0.0f));
}

void Unit::Deselect() {
    renderComponent->SetOutline(false);
}

void Unit::Move() {
    behaviourComponent->Move();
}

BehaviourComponent *Unit::GetBehaviourComponent() const {
    return behaviourComponent;
}

bool Unit::IsMoving() const {
    return behaviourComponent->IsMoving();
}

void Unit::Damage(int damageDealt) {
    behaviourComponent->Damage(damageDealt);
}

bool Unit::IsDead() const {
    return behaviourComponent->IsDead();
}


void Unit::CleanUp() {
    GFXengine::DeleteComponent(renderComponent);
}

UnitComponent::Archetype Unit::Factory::archetype = UnitComponent::Archetype::soldier;
UnitComponent::Race Unit::Factory::race = UnitComponent::Race::human;
int Unit::Factory::regiment = 0;
int Unit::Factory::side = 0;

Unit* Unit::Factory::Spawn(int x, int y, std::string name) {
    return new Unit(name, x,y, archetype, race, side, regiment,false);
}

void Unit::Factory::SetParameter(std::string name, std::string value) {
    // TODO : race and archetype should be more flexible - string not enum to make reading from file easier
    if(name == "race")
    {
        if(value == "human")
            SetRace(UnitComponent::Race::human);
        else if (value == "orc")
            SetRace(UnitComponent::Race::orc);
        else // TODO
            SetRace(UnitComponent::Race::human);
    }
    else if(name == "archetype")
    {
        // TODO
        SetArchetype(UnitComponent::Archetype::soldier);
    }
    else if(name == "regiment")
    {
        int reg;
        std::istringstream s(value);
        s >> reg;
        SetRegiment(reg);
    }
    else if(name == "side")
    {
        int s;
        std::istringstream str(value);
        str >> s;
        SetSide(s);
    }
}

void Unit::Factory::SetRace(UnitComponent::Race r) {
    race = r;
}

void Unit::Factory::SetArchetype(UnitComponent::Archetype a) {
    archetype = a;
}

void Unit::Factory::SetRegiment(int reg) {
    regiment = reg;
}

void Unit::Factory::SetSide(int s) {
    side = s;
}

Unit *Unit::Factory::SpawnCommander(int x, int y, std::string name) {
    return new Unit(name, x,y,archetype,race,side,regiment,true);
}
