
#ifndef TEXTCOMPONENT_H
#define TEXTCOMPONENT_H

#include <glm/glm.hpp>
#include <string>
#include "Component.h"

class TextComponent : public Component{
public:
    TextComponent(Object* p, std::string text, float scale, glm::vec4 color);

    void Update(double dt) override {}

    glm::mat4 GetModelMatrix() const;

    glm::vec4 GetColor() const;
    float GetScale() const;
    std::string GetText() const;

    void SetColor(glm::vec4 newColor);
    void SetScale(float newScale);
    void SetText(std::string newText);

private:
    int fontSize;
    float scale;
    glm::vec4 color;
    std::string text;
};


#endif
