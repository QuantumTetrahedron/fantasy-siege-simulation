
#ifndef UNITCOMPONENT_H
#define UNITCOMPONENT_H


#include "Component.h"
#include <string>

class UnitComponent : public Component{
public:

    enum class Archetype{
        commander, soldier, ranger, unknown
    };
    enum class Race{
        human, orc, elf, unknown
    };

    UnitComponent(Object* parent, std::string name, Archetype a, Race r, int s, int reg, bool isCommander);

    void Update(double dt) override {}

    Archetype GetArchetype() const;
    Race GetRace() const;
    int GetRegiment() const;
    int GetSide() const;
    std::string GetName() const;

    bool IsCommander() const;

private:
    void setArchetype(Archetype a);
    void setRace(Race r);
    void setRegiment(int reg);
    void setSide(int s);

    bool isCommander;
    Archetype archetype;
    Race race;
    int regimment;
    int side;

    std::string name;
};


#endif
