
#ifndef TEXT_H
#define TEXT_H

#include "UIelement.h"
#include "TextComponent.h"

class Text : public UIelement{
public:
    Text(glm::vec2 screenSize, std::string text, glm::vec2 pos, float scale, glm::vec4 color, UIelement* parent);

    void Scale(float s);
    float GetScale();

    void SetText(std::string text);
private:
    TextComponent* textComponent;
};

#endif
