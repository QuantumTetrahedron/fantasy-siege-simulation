
#ifndef COMPONENT_H
#define COMPONENT_H

enum class CompType{
    behaviour,
    render,
    unit,
    sprite,
    text,
    unspecified
};

class Object;

class Component {
public:
    Component(Object* p);
    virtual void Update(double dt) = 0;
    CompType GetType() const;
    const Object* GetParent() const;
protected:
    void SetParent(Object* p);
    CompType type;
    Object* parent;
};


#endif
