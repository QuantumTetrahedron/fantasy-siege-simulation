
#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include <vector>
#include "Field.h"
#include "Unit.h"
#include "Input.h"

class Battlefield {
public:

    static void Load(const char* terrainMapFilePath, const char* unitMapFilePath);

    static glm::vec2 GetSize();

    static Unit* getUnit(int x, int y);

    static void ProcessInput(Input* in);
    static void Update(double dt);

    static bool HasUnit(int x, int y);
    static bool TerrainIsTrespassable(int x, int y);

    enum class State{
        Start, CommandPhase, CommandPhaseEnded, CalculatingMoves, CalculatingMovesEnded, Moving, MovingEnded, FindingTargets, FindingTargetsEnded, Attacking, AttackingEnded
    };

    static glm::vec2 GetFriendlyFlag(int side);
    static glm::vec2 GetEnemyFlag(int side);
    static glm::vec2 GetCommanderOfRegiment(int reg, int side);
    static bool HasCommanderOfRegiment(int reg, int side);
    static void IssueOrderToRegiment(glm::vec2 order, int regiment, int side);
private:

    Battlefield(){}

    static void IssueCommands();
    static void CalculatePenalties();
    static void MoveUnits();
    static void FindTargets();
    static void Attack();
    static void EraseDead();

    static void LoadTerrainFromFile(const char* terrainMapFilePath);
    static void LoadUnitsFromFile(const char* unitMapFilePath);

    static glm::vec2 size;

    static std::vector<Field*> fields;
    static std::vector<Unit*> units;
    static std::map<int, glm::vec2> flags;

    static State state;

    static bool autoPlay;
};


#endif
