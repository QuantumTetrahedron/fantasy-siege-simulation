
#ifndef BOXCOLLIDERCOMPONENT_H
#define BOXCOLLIDERCOMPONENT_H

#include <glm/glm.hpp>
#include <functional>
#include "Component.h"

class BoxColliderComponent : public Component{
public:
    BoxColliderComponent(Object* parent, glm::vec3 objectSize, std::function<void()> collisionCallback);

    void Update(double dt) override {}

    void CheckCollision(glm::vec3 point) const;
    bool Collided(glm::vec3 point) const;
    std::function<void()> OnCollision;
private:
    bool Between(double v, double l, double r) const;

    glm::vec3 size;
};


#endif
