
#include "Ray.h"

Ray::Ray(const Camera &cam, double scrX, double scrY) {
    position = cam.GetPosition();

    double x = 2 * scrX / cam.GetScreenSize().x - 1;
    double y = 1 - 2 * scrY / cam.GetScreenSize().y;

    glm::vec3 ray_nds = glm::vec3(x,y,1.0f);
    glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0f, 1.0f);
    glm::vec4 ray_eye = glm::inverse(cam.GetProjectionMatrix()) * ray_clip;
    ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0f, 0.0f);
    glm::vec4 ray_wor = glm::inverse(cam.GetViewMatrix()) * ray_eye;
    glm::vec3 ray = glm::vec3(ray_wor.x, ray_wor.y, ray_wor.z);
    ray = glm::normalize(ray);

    direction = ray;
}

void Ray::Collide(const std::vector<BoxColliderComponent *> &colliders) {
    for(glm::vec3 s = position; s.y >= -0.5f; s += direction * 0.1f){
        for(BoxColliderComponent* col : colliders){
            if(col->Collided(s)){
                col->OnCollision();
                return;
            }
        }
    }
}
