#ifndef MODEL_H
#define MODEL_H

#include <string>
#include <vector>
#include "Shader.h"
#include "Mesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Model {
public:
    Model(const std::string& path);

    void Draw(const Shader& shader) const;
    void DrawInstanced(const std::vector<glm::mat4>& instanceMats, const Shader& shader, int count) const;

    std::vector<Tex> texturesLoaded;
    std::vector<Mesh> meshes;
    std::string directory;

    void AllocateInstanceBuffers(const std::vector<glm::mat4>& matrices);
private:

    void loadModel(const std::string& path);
    void processNode(aiNode* node, const aiScene* scene);
    Mesh processMesh(aiMesh* mesh, const aiScene* scene);

    std::vector<Tex> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName, bool gammaCorrection);
    unsigned int TextureFromFile(const char* path, const std::string& directory, bool gammaCorrection);
};


#endif
