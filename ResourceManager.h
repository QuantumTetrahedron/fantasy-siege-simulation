
#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H


#include <map>
#include "Model.h"
#include "Shader.h"
#include "Texture.h"

class ResourceManager {
public:

    static Shader GetShader(std::string name);
    static void LoadShader(const char* vertexShaderFilePath, const char* fragmentShaderFilePath, std::string name);
    static void DeleteShader(std::string name);

    static Model GetModel(std::string name);
    static void LoadModel(const char* filePath, std::string name);
    static void DeleteModel(std::string name);

    static Texture GetTexture(std::string name);
    static void LoadTexture(const char* filePath, bool alpha, std::string name);
    static void DeleteTexture(std::string name);

private:
    static std::map<std::string, Shader> shaders;
    static std::map<std::string, Model> models;
    static std::map<std::string, Texture> textures;
};

#endif
