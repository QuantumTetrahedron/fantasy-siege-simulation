
#ifndef TEXTRENDERER_H
#define TEXTRENDERER_H

#include <glm/glm.hpp>
#include <map>
#include "Shader.h"
#include "TextComponent.h"

class TextRenderer {
public:

    void Init();
    void Load(std::string font, unsigned int fontSize);
    void RenderText(std::string text, float x, float y, float scale, glm::vec4 color = glm::vec4(1.0f));
    void RenderText(TextComponent* textComponent);

private:
    struct Character{
        unsigned int TextureID;
        glm::ivec2 Size;
        glm::ivec2 Bearing;
        unsigned int Advance;
    };
    std::map<char, Character> Characters;
    Shader textShader;
    unsigned int VAO, VBO;
};


#endif
