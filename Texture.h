
#ifndef TEXTURE_H
#define TEXTURE_H

class Texture {
public:
    Texture(const char* path, bool alpha);

    void Bind() const;

private:
    unsigned int id;
    unsigned int width, height;
    unsigned int dataFormat, internalFormat;
    unsigned int wrapS, wrapT, filterMin, filterMax;

    void Generate(unsigned int width, unsigned int height, unsigned char* data);
};


#endif
