
#include <glm/common.hpp>
#include "Personality.h"

void Personality::Normalize() {
    float sum = 0.0f;
    for(auto p : weights){
        sum += p.second;
    }
    for(auto p : weights){
        p.second /= sum;
    }
}

void Personality::Set(std::string name, float value) {
    weights[name] = value;
}

float Personality::Get(std::string name) const {
    return weights.at(name);
}


