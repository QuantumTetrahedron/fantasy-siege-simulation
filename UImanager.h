
#ifndef UIMANAGER_H
#define UIMANAGER_H

#include <vector>
#include "SpriteRenderer.h"
#include "SpriteComponent.h"
#include "Camera.h"
#include "TextComponent.h"
#include "TextRenderer.h"

class UImanager {
public:
    void Init();
    void Draw();

    void RegisterMainCamera(Camera* c);
    void RegisterComponent(SpriteComponent* c);
    void DeleteComponent(SpriteComponent* c);
    void RegisterComponent(TextComponent* c);
    void DeleteComponent(TextComponent* c);
private:
    Camera* cam;
    SpriteRenderer renderer;
    TextRenderer textRenderer;
    std::vector<SpriteComponent*> sprites;
    std::vector<TextComponent*> texts;
};


#endif
